$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    bindSellQuantity();
    $("#tbodyProduct").on("click", "a.link_delete", function(){
        if(confirm('Bạn có thực sự muốn xóa ?')){
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    ProductId: id,
                    StatusId: 0
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) $('tr#product_' + id).remove();
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    }).on('click', '.link_import', function() {
        var productId = $(this).attr('data-id');
        $('select#importProductId').val(productId).trigger('change');
        $('input#quantityPlus').val(0);
        var unitId = $('select#importProductId').find('option[value="' + productId + '"]').attr('data-id');
        $('#spanUnitName').text($('select#unitId1 option[value="' + unitId + '"]').text().trim());
        $('#modalImport').modal('show');
        return false;
    }).on('click', '.aProductCode', function(){
        var productId = $(this).attr('data-id');
        $('input#productId').val(productId);
        var tr = $('tr#product_' + productId);
        $('input#productName').val(tr.find('.tdProductName').text().trim());
        $('select#productTypeId1').val(tr.find('input.productTypeId').val()).trigger('change');
        $('select#unitId1').val(tr.find('input.unitId').val()).trigger('change');
        $('input#quantity').val(tr.find('.tdQuantity').text().trim());
        $('#modalProduct').modal('show');
        return false;
    });
    $('#aAddProduct').click(function() {
        $('input#productName').val('');
        $('input#quantity, input#productId').val(0);
        $('select#productTypeId1, select#unitId1').val(0).trigger('change');
        $('#modalProduct').modal('show');
        return false;
    });
    $('#modalProduct').on('keydown', 'input#quantity', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input#quantity', function () {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    });
    $('#btnUpdateProduct').click(function(){
        var formId = '#modalProduct';
        if(validateEmpty(formId) && validateNumber(formId, true, 'không được bỏ trống')){
            var productId = parseInt($('input#productId').val());
            var btn = $(this);
            btn.prop("disabled", true);
            $.ajax({
                type: "POST",
                url: $('input#updateProductUrl').val(),
                data: {
                    ProductId: productId,
                    ProductName: $('input#productName').val().trim(),
                    ProductTypeId: $('select#productTypeId1').val(),
                    UnitId: $('select#unitId1').val(),
                    StatusId: $('input#statusId').val(),
                    Quantity: replaceCost($('input#quantity').val(), true)
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        var data = json.data;
                        var productTypeName = $('select#productTypeId1 option[value="' + data.ProductTypeId + '"]').text().trim();
                        var unitName = $('select#unitId1 option[value="' + data.UnitId + '"]').text().trim();
                        if(productId > 0){
                            var tr = $('tr#product_' + productId);
                            tr.find('.tdProductName').text(data.ProductName);
                            tr.find('.tdProductTypeName').text(productTypeName);
                            tr.find('.tdUnitName').text(unitName);
                            tr.find('.tdQuantity').text(formatDecimal(data.Quantity));
                            tr.find('input.productTypeId').val(data.ProductTypeId);
                            tr.find('input.unitId').val(data.UnitId);
                        }
                        else{
                            var html = '<tr id="product_' + data.ProductId + '">';
                            html += '<td class="text-center"><a href="javascript:void(0)" class="aProductCode" data-id="' + data.ProductId + '">' + data.ProductCode + '</a></td>';
                            html += '<td class="tdProductName">' + data.ProductName + '</td>';
                            html += '<td class="tdProductTypeName">' + productTypeName + '</td>';
                            html += '<td class="tdUnitName">' + unitName + '</td>';
                            html += '<td class="text-center tdQuantity">' + formatDecimal(data.Quantity) + '</td>';
                            html += '<td class="text-center"></td>';
                            html += '<td class="actions"><a href="javascript:void(0)" class="link_delete" data-id="' + data.ProductId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>';
                            html += '<a href="javascript:void(0)" class="link_import" data-id="' + data.ProductId + '" title="Nhập hàng"><i class="fa fa-product-hunt"></i></a>';
                            html += '<input type="text" hidden="hidden" class="productTypeId" value="' + data.ProductTypeId + '">';
                            html += '<input type="text" hidden="hidden" class="unitId" value="' + data.UnitId + '"></td></tr>';
                            $("#tbodyProduct").prepend(html);
                        }
                        $('#modalProduct').modal('hide');
                    }
                    btn.prop("disabled", false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop("disabled", false);
                }
            });
        }
        return false;
    });
    $('#aAddImport').click(function() {
        $('select#importProductId').val(0).trigger('change');
        $('input#quantityPlus').val(0);
        $('#spanUnitName').text('');
        $('input#comment').val('');
        $('#modalImport').modal('show');
        return false;
    });
    $('#modalImport').on('keydown', 'input#quantityPlus', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input#quantityPlus', function () {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    });
    $('select#importProductId').change(function(){
        var productId = $(this).val();
        if(productId == '0') $('#spanUnitName').text('');
        else{
            var unitId = $(this).find('option[value="' + productId + '"]').attr('data-id');
            $('#spanUnitName').text($('select#unitId1 option[value="' + unitId + '"]').text().trim());
        }
    });
    $('#btnInsertImport').click(function(){
        var flag = true;
        var productId = $('select#importProductId').val();
        if(productId == 0){
            showNotification('Vui lòng chọn sản phẩm', 0);
            flag = false;
        }
        var quantity = replaceCost($('input#quantityPlus').val(), true);
        if(quantity <= 0){
            showNotification('Vui lòng nhập số lượng', 0);
            flag = false;
        }
        if(flag){
            var btn = $(this);
            btn.prop("disabled", true);
            $.ajax({
                type: "POST",
                url: $('input#insertImportUrl').val(),
                data: {
                    ProductId: productId,
                    Quantity: quantity,
                    ImportDate: $('input#importDate').val(),
                    ImportTypeId: 1,
                    OrderId: 0,
                    Comment: $('input#comment').val().trim()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        $('tr#product_' + productId).find('.tdQuantity').text(formatDecimal(json.data));
                        $('#modalImport').modal('hide');
                    }
                    btn.prop("disabled", false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop("disabled", false);
                }
            });
        }
        return false;
    });
});

function bindSellQuantity() {
    var productIds = $('input#productIds').val().trim();
    if(productIds != '' && productIds != '[]'){
        $.ajax({
            type: "POST",
            url: $('input#getSellQuantityUrl').val(),
            data: {
                ProductIds: productIds
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1){
                    var data = json.data;
                    for(var i = 0; i < data.length; i++) $('tr#product_' + data[i].ProductId).find('.tdSellQuantity').html(formatDecimal(data[i].SumSellQuantity));
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }
}