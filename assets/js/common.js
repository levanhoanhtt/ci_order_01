$(document).ready(function() {
    var siteName = $('input#siteName').val();
    if(siteName != ''){
        siteName = $('title').text() + ' - ' + siteName;
        $('title').text(siteName);
        $('input#siteName').val(siteName);
    }
    if($('select.select2').length > 0) $('select.select2').select2();
    if($('div.divTable').length > 0) {
        if($(window).width() > 760) $('div.divTable').removeClass('table-responsive');
        else $('div.divTable').addClass('table-responsive');
        $(window).resize(function () {
            if($(window).width() > 760) $('div.divTable').removeClass('table-responsive');
            else $('div.divTable').addClass('table-responsive');
        });
    }
    $(document).ajaxStart(function() { Pace.restart(); });

    var curentPathName = window.location.pathname;
    var rootPath = $('#roorPath').val();
    curentPathName = curentPathName.replace(rootPath, '');
    var hostname = window.location.hostname;
    //admin menu
    if($('#navbar-collapse').length > 0){
        $('#navbar-collapse ul li a').each(function() {
            var pageLink = $(this).attr("href");
            pageLink = pageLink.replace('https://', '');
            pageLink = pageLink.replace('http://', '');
            pageLink = pageLink.replace(hostname, '');
            pageLink = pageLink.replace(rootPath, '');
            if(pageLink == curentPathName){
                $(this).parent('li').addClass('active');
                $(this).parent('li').parent('ul.dropdown-menu').parent('li').addClass('active');
                return false;
            }
        });
    }
});

/* type = 1 - success
other - error
*/
function showNotification(msg, type){
    var typeText = 'error';
    if(type == 1) typeText = 'success';
    var notice = new PNotify({
        title: 'Thông báo',
        text: msg,
        type: typeText,
        delay: 2000,
        addclass: 'stack-bottomright',
        stack: {"dir1": "up", "dir2": "left", "firstpos1": 15, "firstpos2": 15}
    });
}

function redirect(reload, url){
    if(reload){
        window.setTimeout(function () {
            window.location.reload(true);
        }, 2000);
    }
    else{
        window.setTimeout(function() {
            window.location.href = url;
        }, 2000);
    }
}

//price format
function replaceCost(cost, isInt) {
    cost = cost.toString().replace(/\,/g, '');
    if(cost == '') cost = 0;
    if(isInt) return parseInt(cost);
    else  return parseFloat(cost);
}

function formatDecimal(value){
    value = value.toString().replace(/\,/g, '');
    while(value.length > 1 && value[0] == '0' && value[1] != '.') value = value.substring(1);
    if(value != '' && value != '0'){
        if(value[value.length - 1] != '.') {
            if(value.indexOf('.00') > 0) value = value.substring(0, value.length - 3);
            value = addCommas(value);
            return value;
        }
        else return value;
    }
    else return 0;
}

function addCommas(nStr){
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function checkKeyCodeNumber(e){
    return !((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8 ||  e.keyCode == 35 || e.keyCode == 36 || e.keyCode == 37 || e.keyCode == 39 || e.keyCode == 46);
}

function scrollTo(eleId){
    $('html, body').animate({
        scrollTop: $(eleId).offset().top - 200
    }, 1000);
    $(eleId).focus();
}

//validate
function validateEmpty(container) {
    var flag = true;
    $(container + ' .hmdrequired').each(function () {
        if($(this).val().trim() == '') {
            showNotification($(this).attr('data-field') + ' không được bỏ trống', 0);
            $(this).focus();
            flag = false;
            return false;
        }
    });
    return flag;
}

function validateNumber(container, isInt, msg) {
    if(typeof(msg) == 'undefined') msg = ' không được bé hơn 0';
    var flag = true;
    var value = 0;
    $(container + ' .hmdrequiredNumber').each(function () {
        value = replaceCost($(this).val(), isInt);
        if(value <= 0) {
            showNotification($(this).attr('data-field') + ' ' + msg, 0);
            $(this).focus();
            flag = false;
            return false;
        }
    });
    return flag;
}

function checkEmptyEditor(text) {
    text = text.replace(/\&nbsp;/g, '').replace(/\<p>/g, '').replace(/\<\/p>/g, '').trim();
    return text.length > 0;
}

function makeSlug(str){
    var slug = str.trim().toLowerCase();
    // change vietnam character
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    // remove special character
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    // change space to -
    slug = slug.replace(/ /gi, "-");
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    return slug;
}

function progressBarUpload(progressBarId) {
    var xhr = new window.XMLHttpRequest();
    xhr.upload.addEventListener("progress", function (evt) {
        if(evt.lengthComputable) {
            var percentComplete = evt.loaded / evt.total;
            percentComplete = parseInt(percentComplete * 100);
            var prb = $('#' + progressBarId + ' .progress-bar');
            prb.text(percentComplete + '%');
            prb.css({
                width: percentComplete + '%'
            });
            if(percentComplete === 100) {
                setTimeout(function () {
                    prb.text('Tải ảnh lên hoàn thành');
                }, 1000);
            }
        }
    }, false);
    return xhr;
}

function validateImage(fileName) {
    var typeFile = getFileExtension(fileName);
    var whiteList = ['jpeg', 'jpg', 'png', 'bmp'];
    if(whiteList.indexOf(typeFile) === -1) {
        showNotification('Tệp tin phải là ảnh có định dạng , jpeg/jpg/png/bmp', 0);
        return false;
    }
    return true;
}

function getFileExtension(fileName) {
    return fileName.split(".").pop().toLowerCase();
}

function getCurrentDateTime(){
    var now = new Date();
    var date = now.getDate();
    var month = now.getMonth() + 1;
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    if(date < 10) date = '0' + date;
    if(month < 10) month = '0' + month;
    if(hour < 10) hour = '0' + hour;
    if(minute < 10) minute = '0' + minute;
    if(second < 10) second = '0' + second;
    return date + "/" + month + "/" + now.getFullYear() + " " + hour + ":" + minute + ":" + second;
}

//pagging
function pagging(pageId){
    $('input#pageId').val(pageId);
    $('input#submit').trigger('click');
}

function changeProvince(provinceIdStr, districtIdStr, wardIdStr) {
    if(typeof(provinceIdStr) == 'undefined'){
        provinceIdStr = 'provinceId';
        districtIdStr = 'districtId';
        wardIdStr = 'wardId';
    }
    var selPro = $('select#' + provinceIdStr);
    if(selPro.length > 0) {
        var selDistrict = $('select#' + districtIdStr);
        var selWard = $('select#' + wardIdStr);
        selDistrict.find('option').hide();
        selDistrict.find('option[value="0"]').show();
        var provinceId = selPro.val();
        if(provinceId != '0') selDistrict.find('option[data-id="' + provinceId + '"]').show();
        var txtAll = selWard.find('option[value="0"]').text().trim();
        selPro.change(function () {
            selDistrict.find('option').hide();
            provinceId = $(this).val();
            if(provinceId != '0') selDistrict.find('option[data-id="' + provinceId + '"]').show();
            selDistrict.val('0');
            selWard.html('<option value="0">' + txtAll + '</option>');
        });
        var districtId = selDistrict.val();
        if(districtId != '0') getListWard(districtId, selWard, selWard.attr('data-id'), txtAll);
        selDistrict.change(function () {
            districtId = $(this).val();
            if(districtId != '0') getListWard(districtId, selWard, 0, txtAll);
        });
    }
}

function getListWard(districtId, selWard, wardId, txtAll){
    $.ajax({
        type: "POST",
        url: $('input#getListWardUrl').val(),
        data: {
            DistrictId: districtId
        },
        success: function (response) {
            var data = $.parseJSON(response);
            var html = '<option value="0">' + txtAll + '</option>';
            for(var i = 0; i < data.length; i++) html += '<option value="' + data[i].WardId + '">' + data[i].WardName + '</option>';
            selWard.html(html).val(wardId);
        },
        error: function (response) {
            selWard.html('<option value="0">' + txtAll + '</option>');
        }
    });
}