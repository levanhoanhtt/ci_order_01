$(document).ready(function(){
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    changeProvince();
    $('.chooseImage').click(function(){
        $('#inputUserImage').trigger('click');
    });
    $('#inputUserImage').change(function (e) {
        var file = this.files[0];
        if(!validateImage(file.name)) return;
        var reader = new FileReader();
        reader.addEventListener("load", function () {
            $('#userProgress').show();
            $.ajax({
                xhr: function () {
                    return progressBarUpload('userProgress');
                },
                type: 'POST',
                url: $('input#uploadFileUrl').val(),
                data: {
                    FileBase64: reader.result,
                    FileTypeId: 2
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        $('input#avatar').val(json.data);
                        $('img#imgAvatar').attr('src', json.data);
                    }
                    else showNotification(json.message, json.code);
                    $('#userProgress').hide();
                },
                error: function (response) {
                    $('#userProgress').hide();
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }, false);
        if (file) reader.readAsDataURL(file);
    });
    $('a#generatorPass').click(function(){
        var pass = randomIntFromInterval(100000000, 999999999);
        $('input#newPass').val(pass);
        $('input#rePass').val(pass);
        return false;
    });
    $('#btnSubmit').click(function (){
        var formId = '#userForm';
        if(validateEmpty(formId) && validateNumber(formId, true, 'không được bỏ trống')){
            var flag = $('input#newPass').val().length > 0;
            if(flag){
                if ($('input#newPass').val() != $('input#rePass').val()) {
                    showNotification('Mật khẩu không trùng', 0);
                    return false;
                }
            }
            if($('input#userName').val().trim().indexOf(' ') >= 0){
                showNotification('Tên đăng nhập không được có khoảng trằng', 0);
                $('input#userName').focus();
                return false;
            }
            var btn = $(this);
            btn.prop("disabled", true);
            var form = $(formId);
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if(flag){
                            $('input#userPass').val('');
                            $('input#newPass').val('');
                            $('input#rePass').val('');
                        }
                        if($('input#isUserProfile').val() == '0'){
                            if($('input#userId').val() == '0') redirect(false, $('#userEditUrl').val() + json.data);
                        }
                    }
                    btn.prop("disabled", false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop("disabled", false);
                }
            });
        }
        return false;
    });
});

function randomIntFromInterval(min,max){
    return Math.floor(Math.random() * (max - min + 1) + min);
}