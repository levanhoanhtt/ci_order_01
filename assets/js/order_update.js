$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('select#customerId').change(function(){
        $('#tbodyProductSell, #tbodyProductReturn').html('');
        var divCustomer = $('#divCustomer');
        divCustomer.find('h3.widget-user-username').text('');
        divCustomer.find('h5.widget-user-desc').text('');
        divCustomer.find('ul.nav-stacked').html('');
        var customerId = parseInt($(this).val());
        if(customerId > 0){
            var pCustomer = $('#pCustomer_' + customerId);
            divCustomer.find('h3.widget-user-username').text(pCustomer.attr('data-name'));
            var html = '<li><a href="javascript:void(0)">Số điện thoại <span class="pull-right badge bg-aqua">' + pCustomer.attr('data-phone') + '</span></a></li>';
            html += '<li><a href="javascript:void(0)">Email <span class="pull-right badge bg-aqua">' + pCustomer.attr('data-email') + '</span></a></li>';
            html += '<li><a href="javascript:void(0)">Địa chỉ <span class="pull-right badge bg-aqua" id="spanAddress">' + pCustomer.text() + ', Thị trấn Hà Trung<br/>Quận/ Huyện: Huyện Hà Trung<br/>Tỉnh/ Thành phố: Thanh Hóa</span></a><div class="clearfix"></div></li>';
            html += '<li><a href="javascript:void(0)">Đánh giá vị trí cửa hàng <span class="pull-right badge bg-aqua" id="spanStoreReviewTypeName"></span></a></li>';
            html += '<li><a href="javascript:void(0)">Mức độ thân thiết <span class="pull-right badge bg-aqua" id="spanIntimacyTypeName"></span></a></li>';
            divCustomer.find('ul.nav-stacked').html(html);
            $.ajax({
                type: "POST",
                url: $('input#getCustomerInfoUrl').val(),
                data: {
                    CustomerId: customerId,
                    CustomerTypeId: pCustomer.attr('data-type'),
                    ProvinceId: pCustomer.attr('data-province'),
                    DistrictId: pCustomer.attr('data-district'),
                    WardId: pCustomer.attr('data-ward'),
                    StoreReviewTypeId: pCustomer.attr('data-review'),
                    IntimacyTypeId: pCustomer.attr('data-intimacy'),
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        var data = json.data;
                        divCustomer.find('h5.widget-user-desc').text(data.CustomerTypeName);
                        divCustomer.find('#spanStoreReviewTypeName').text(data.StoreReviewTypeName);
                        divCustomer.find('#spanIntimacyTypeName').text(data.IntimacyTypeName);
                        html = pCustomer.text();
                        if(html != '') html += ', ';
                        html += data.WardName;
                        if(html != '') html += '<br/>';
                        if(data.DistrictName != '') html += 'Quận/ Huyện: ' + data.DistrictName + '<br/>';
                        html += 'Tỉnh/ Thành phố: ' + data.ProvinceName;
                        divCustomer.find('#spanAddress').html(html);
                    }
                    else showNotification(json.message, json.code);
                    divCustomer.show();
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    divCustomer.show();
                }
            });
        }
        else divCustomer.hide();
    });
    $('select#productId').change(function() {
        //chooseProduct($(this), 1);
        var productId = parseInt($(this).val());
        if(productId > 0){
            var tbody = $('#tbodyProduct');
            var inputProduct = $('input#product_' + productId);
            var tr = tbody.find('#trProduct_' + productId);
            if(tr.length > 0){
                /*var quantity = replaceCost(tr.find('input.sellQuantity').val(), true) + 1;
                tr.find('input.sellQuantity').val(formatDecimal(quantity));*/
                showNotification('Bạn đã chọn sản phẩm này rồi', 0);
            }
            else{
                var inputProductIndex = $('input#productIndex');
                var productReturnIndex = parseInt(inputProductIndex.val());
                productReturnIndex++;
                $('input#productSellIndex').val(productReturnIndex);
                var html = '<tr id="trProduct_' + productId + '" data-id="' + productId + '">';
                html += '<td class="text-center">' + productReturnIndex + '</td>';
                html += '<td class="text-center">' + inputProduct.attr('data-code') + '</td>';
                html += '<td>' + inputProduct.val() + '</td><td>' + $('input#productTypeName_' + inputProduct.attr('data-type')).val() + '</td>';
                html += '<td><input type="text" class="form-control quantity sellQuantity" value="0"></td>';
                html += '<td><input type="text" class="form-control quantity returnQuantity" value="0"></td>';
                html += '<td>' + $('input#unitName_' + inputProduct.attr('data-unit')).val() + '</td>';
                html += '<td class="actions"><a href="javascript:void(0)" class="link_delete" title="Xóa"><i class="fa fa-trash-o"></i></a></td></tr>';
                tbody.append(html);
            }
            $(this).val(0).trigger('change');
        }
    });
    $('#tbodyProduct').on('keydown', 'input.quantity', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input.quantity', function () {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    }).on('click', '.link_delete', function() {
        $(this).parent().parent().remove();
        return false;
    });
    $('#btnSubmitOrder').click(function(){
        var btn = $(this);
        var customerId = $('select#customerId').val();
        if(customerId == '0'){
            showNotification('Vui lòng chọn Khách hàng');
            return false;
        }
        var products = [];
        var flag = true;
        var tr, sellQuantity, returnQuantity;
        $('#tbodyProduct tr').each(function(){
            tr = $(this);
            sellQuantity = replaceCost(tr.find('input.sellQuantity').val(), true);
            returnQuantity = replaceCost(tr.find('input.returnQuantity').val(), true);
            if(sellQuantity <= 0 && returnQuantity <= 0){
                showNotification('Vui lòng nhập số lượng sản phẩm');
                flag = false;
                return false;
            }
            else{
                products.push({
                    ProductId: parseInt(tr.attr('data-id')),
                    SellQuantity: sellQuantity,
                    ReturnQuantity: returnQuantity
                });
            }
        });
        if(flag && products.length == 0){
            showNotification('Vui lòng chọn Sản phẩm');
            return false;
        }
        btn.prop("disabled", true);
        $.ajax({
            type: "POST",
            url: $('input#updateOrderUrl').val(),
            data: {
                OrderId: 0,
                CustomerId: customerId,
                StatusId: 2,
                OrderDate: $('input#orderDate').val().trim(),
                Products: products
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) redirect(false, $('input#orderListUrl').val());
                else btn.prop("disabled", false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop("disabled", false);
            }
        });
        return false;
    });
});