$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    changeProvince();
    $('#btnSubmit').click(function (){
        var formId = '#customerForm';
        if(validateEmpty(formId) && validateNumber(formId, true, 'không được bỏ trống')){
            var btn = $(this);
            btn.prop("disabled", true);
            var form = $(formId);
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if($('input#customerId').val() == '0') redirect(false, $('#customerEditUrl').val() + json.data);
                    }
                    btn.prop("disabled", false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop("disabled", false);
                }
            });
        }
        return false;
    });
});