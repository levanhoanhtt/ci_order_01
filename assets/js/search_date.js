$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('select#dayCompareTypeId').change(function(){
        var id = parseInt($(this).val());
        if(id == 1) $('.divDate').show();
        else if(id == 2 || id == 4){
            $('#divBeginDate').show();
            $('#divEndDate').hide();
        }
        else if(id == 3){
            $('#divEndDate').show();
            $('#divBeginDate').hide();
        }
    });
});