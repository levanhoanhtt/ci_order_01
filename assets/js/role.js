$(document).ready(function(){
    $('select#roleId option[value="4"]').hide();
    $('#checkAll, #unCheckAll, #btnUpdate').hide();
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('#checkAll').click(function(){
        $('input.iCheck').iCheck('check');
        $(this).hide();
        $('#unCheckAll').show();
        return false;
    });
    $('#unCheckAll').click(function(){
        $('input.iCheck').iCheck('uncheck');
        $(this).hide();
        $('#checkAll').show();
        return false;
    });
    $('select#roleId').change(function(){
        $('input.iCheck').iCheck('uncheck');
        var roleId = parseInt($(this).val());
        if(roleId > 0){
            $('#checkAll, #unCheckAll, #btnUpdate').show();
            $.ajax({
                type: "POST",
                url: $('input#getActionUrl').val(),
                data: {
                    RoleId: roleId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1){
                        var data = json.data;
                        for(var i = 0; i < data.length; i++) $('input#cbAction_' + data[i].ActionId).iCheck('check');
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        else $('#checkAll, #unCheckAll, #btnUpdate').hide();
    });
    $('#btnUpdate').click(function(){
        var roleId = parseInt($('select#roleId').val());
        if(roleId > 0){
            var actionIds = [];
            $('.icheckbox_square-blue').each(function(){
                if($(this).hasClass('checked')) actionIds.push($(this).find('input.iCheck').val());
            });
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateRoleUrl').val(),
                data: {
                    RoleId: roleId,
                    ActionIds: JSON.stringify(actionIds)
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
    })
});