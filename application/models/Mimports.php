<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mimports extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "imports";
        $this->_primary_key = "ImportId";
    }

    public function insert($postData){
        $this->db->trans_begin();
        $importId = $this->save($postData);
        if($postData['ImportTypeId'] == 2) $query =  "UPDATE products SET Quantity = Quantity - ? WHERE ProductId = ?";
        else $query = "UPDATE products SET Quantity = Quantity + ? WHERE ProductId = ?";
        $this->db->query($query, array(intval($postData['Quantity']), $postData['ProductId']));
        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $importId;
        }
    }
}