<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Munits extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "units";
        $this->_primary_key = "UnitId";
    }
}
