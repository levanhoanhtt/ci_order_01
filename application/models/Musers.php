<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Musers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "users";
        $this->_primary_key = "UserId";
    }

    public function login($userName, $userPass){
        if(!empty($userName) && !empty($userPass)){
            $query = "SELECT * FROM users WHERE UserPass=? AND UserStatusId=? AND (UserName=? OR PhoneNumber=?) LIMIT 1";
            $users = $this->getByQuery($query, array(md5($userPass), STATUS_ACTIVED, $userName, $userName));
            if (!empty($users)) return $users[0];
        }
        return false;
    }

    public function checkExist($userId, $roleId, $userName, $phoneNumber, $email){
        $query = "SELECT UserId FROM users WHERE UserId != ? AND RoleId = ? AND UserName = ? AND UserStatusId = ?";
        $param = array($userId, $roleId, $userName, STATUS_ACTIVED);
        $flag = true;
        if(!empty($phoneNumber) && !empty($email)){
            $query .= " AND (PhoneNumber = ? OR Email = ?) LIMIT 1";
            $param[] = $phoneNumber;
            $param[] = $email;
        }
        elseif(!empty($phoneNumber)){
            $query .= " AND PhoneNumber = ? LIMIT 1";
            $param[] = $phoneNumber;
        }
        elseif(!empty($email)){
            $query .= " AND Email = ? LIMIT 1";
            $param[] = $email;
        }
        else $flag = false;
        if($flag){
            $users = $this->getByQuery($query, $param);
            if(!empty($users)) return true;
        }
        return false;
    }

    public function getCount($postData){
        $query = "UserStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1, $select = '*'){
        $query = "SELECT {$select} FROM users WHERE UserStatusId > 0" . $this->buildQuery($postData) . ' ORDER BY UserId DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['UserName']) && !empty($postData['UserName'])) $query.=" AND UserName LIKE '%{$postData['UserName']}%'";
        if(isset($postData['FullName']) && !empty($postData['FullName'])) $query.=" AND FullName LIKE '%{$postData['FullName']}%'";
        if(isset($postData['Email']) && !empty($postData['Email'])) $query.=" AND Email LIKE '%{$postData['Email']}%'";
        if(isset($postData['PhoneNumber']) && !empty($postData['PhoneNumber'])) $query.=" AND PhoneNumber LIKE '%{$postData['PhoneNumber']}%'";
        if(isset($postData['RoleId']) && $postData['RoleId'] > 0) $query.=" AND RoleId = ".$postData['RoleId'];
        if(isset($postData['GenderId']) && $postData['GenderId'] > 0) $query.=" AND GenderId = ".$postData['GenderId'];
        if(isset($postData['UserStatusId']) && $postData['UserStatusId'] > 0) $query.=" AND UserStatusId = ".$postData['UserStatusId'];
        if(isset($postData['ProvinceId']) && $postData['ProvinceId'] > 0) $query.=" AND ProvinceId = ".$postData['ProvinceId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND DATE(CrDateTime) >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND DATE(CrDateTime) <= '{$postData['EndDate']}'";
        if(isset($postData['RoleIds']) && !empty($postData['RoleIds'])) $query.=" AND RoleId IN({$postData['RoleIds']})";
        return $query;
    }

    public function getListByRole($roleIds, $isAll = false){
        $retVal = array();
        $query = 'SELECT UserId, FullName, RoleId, Email, PhoneNumber, Avatar, UserStatusId FROM users WHERE UserStatusId IN(2, 4)';
        if($isAll) $retVal = $this->getByQuery($query);
        else{
            if(is_numeric($roleIds) && $roleIds > 0) $retVal = $this->getByQuery($query. ' AND RoleId = ?', array($roleIds));
            elseif(is_array($roleIds) && !empty($roleIds)) $retVal = $this->getByQuery($query . ' AND RoleId IN ?', array($roleIds));
        }
        return $retVal;
    }

    public function selectHtml($listUsers, $selectName, $objId = 0, $isAll = false, $txtAll = "", $isDisable = false, $isShowPhone = true){
        $id = str_replace('[]', '', lcfirst($selectName));
        echo '<select class="form-control select2" name="'.$selectName.'" id="'.$id.'">';
        if($isAll){
            if(empty($txtAll)) echo '<option value="0">Tất cả</option>';
            else echo '<option value="0">'.$txtAll.'</option>';
        }
        $isSelectMultiple = is_array($objId);
        foreach($listUsers as $obj){
            $selected = '';
            if(!$isSelectMultiple) {
                if($obj['UserId'] == $objId) $selected = ' selected="selected"';
            }
            elseif(in_array($obj['UserId'], $objId)) $selected = ' selected="selected"';
            if($isDisable && $obj['UserStatusId'] != STATUS_ACTIVED) $selected .= ' disabled';
            if($isShowPhone && !empty($obj['PhoneNumber'])) echo '<option value="'.$obj['UserId'].'"'.$selected.'>'.$obj['FullName'].' - '.$obj['PhoneNumber'].'</option>';
            else echo '<option value="'.$obj['UserId'].'"'.$selected.'>'.$obj['FullName'].'</option>';
        }
        echo '</select>';
    }
}