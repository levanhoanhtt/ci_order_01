<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproducttypes extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "producttypes";
        $this->_primary_key = "ProductTypeId";
    }
}
