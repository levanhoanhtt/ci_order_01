<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdistricts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "districts";
        $this->_primary_key = "DistrictId";
    }

    public function getList($provinceId = 0){
        if($provinceId > 0) return $this->getBy(array('ProvinceId' => $provinceId), false, "DisplayOrder", 'DistrictId, DistrictName', 0, 0, 'asc');
        return $this->get(0, false, "DisplayOrder", '', 0, 0, 'asc');
    }

    public function selectHtml($districtId = 0, $selectName = 'DistrictId', $listDistricts = array(), $txtAll = '--Chọn--'){
        if(empty($listDistricts)) $listDistricts = $this->get(0, false, "DisplayOrder", '', 0, 0, 'asc');
        $retVal = '<select class="form-control" name="'.$selectName.'" id="'.lcfirst($selectName).'"><option value="0" data-id="0">'.$txtAll.'</option>';
        foreach($listDistricts as $d) $retVal .= '<option value="'.$d['DistrictId'].'" data-id="'.$d['ProvinceId'].'"'.($d['DistrictId'] == $districtId ? ' selected="selected"' : '').'>'.$d['DistrictName'].'</option>';
        $retVal .= '</select>';
        return $retVal;
    }
}