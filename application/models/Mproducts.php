<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproducts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "products";
        $this->_primary_key = "ProductId";
    }

    public function checkExist($productId, $productName, $productTypeId){
        $products = $this->getByQuery("SELECT ProductId FROM products WHERE ProductId != ? AND StatusId = ? AND ProductName = ? AND ProductTypeId = ? LIMIT 1", array($productId, STATUS_ACTIVED, $productName, $productTypeId));
        if(!empty($products)) return true;
        return false;
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM products WHERE StatusId > 0" . $this->buildQuery($postData) . ' ORDER BY ProductId DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['ProductCode']) && !empty($postData['ProductCode'])) $query.=" AND ProductCode LIKE '%{$postData['ProductCode']}%'";
        if(isset($postData['ProductName']) && !empty($postData['ProductName'])) $query.=" AND ProductName LIKE '%{$postData['ProductName']}%'";
        if(isset($postData['ProductTypeId']) && $postData['ProductTypeId'] > 0) $query.=" AND ProductTypeId = ".$postData['ProductTypeId'];
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND StatusId = ".$postData['StatusId'];
        if(isset($postData['UnitId']) && $postData['UnitId'] > 0) $query.=" AND UnitId = ".$postData['UnitId'];
        return $query;
    }

    public function getQuantityByIds($productIds){
        $retVal = array();
        foreach($productIds as $productId) $retVal[$productId] = 0;
        $products = $this->getByQuery('SELECT ProductId, Quantity FROM products WHERE ProductId IN ? AND StatusId = ?', array($productIds, STATUS_ACTIVED));
        foreach($products as $p) $retVal[$p['ProductId']] = $p['Quantity'];
        return $retVal;
    }

    public function getSellQuantity($productIds){
        return $this->getByQuery("SELECT ProductId, SUM(SellQuantity) AS SumSellQuantity FROM orderproducts WHERE ProductId IN ? AND StatusId = ? GROUP BY ProductId", array($productIds, STATUS_ACTIVED));
    }

    public function getByOrderId($orderId){
        $query = "SELECT products.ProductId, ProductCode, ProductName, ProductTypeName, UnitName, SellQuantity, ReturnQuantity FROM products
                  INNER JOIN orderproducts ON orderproducts.ProductId = products.ProductId
                  INNER JOIN producttypes ON producttypes.ProductTypeId = products.ProductTypeId
                  INNER JOIN units ON units.UnitId = products.UnitId
                  WHERE orderproducts.OrderId = ? AND orderproducts.StatusId = ?";
        return $this->getByQuery($query, array($orderId, STATUS_ACTIVED));
    }

    public function update($postData, $productId = 0){
        $isInsert = $productId == 0;
        $this->db->trans_begin();
        $productId = $this->save($postData, $productId, array('UpdateUserId', 'UpdateDateTime'));
        if($isInsert) $this->save(array('ProductCode' => $this->genProductCode($productId)), $productId);
        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $productId;
        }
    }

    public function genProductCode($productId){
        return 'SP-' . (10000 + $productId);
    }
}