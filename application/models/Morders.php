<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Morders extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "orders";
        $this->_primary_key = "OrderId";
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1, $joins = array()){
        if(empty($joins)) $query = "SELECT * FROM orders";
        else{
            $selectFields = array();
            $query = 'SELECT orders.*,{selectField} FROM orders';
            $flag1 = false;
            $flag2 = false;
            foreach($joins as $join){
                if($join == 'orderproducts'){
                    $selectFields[] = 'orderproducts.ProductId, orderproducts.SellQuantity, orderproducts.ReturnQuantity';
                    $query .= ' INNER JOIN orderproducts ON orderproducts.OrderId = orders.OrderId';
                    $flag1 = true;
                }
                if($flag1 && $join == 'products'){
                    $selectFields[] = 'products.ProductName, products.ProductTypeId, products.UnitId';
                    $query .= ' INNER JOIN products ON orderproducts.ProductId = products.ProductId';
                }
                if($join == 'customers'){
                    $selectFields[] = 'customers.FullName, customers.PhoneNumber, customers.Email, customers.ProvinceId, customers.DistrictId, customers.WardId, customers.Address';
                    $query .= ' INNER JOIN customers ON orders.CustomerId = customers.CustomerId';
                    $flag2 = true;
                }
                if($flag2 && $join == 'wards'){
                    $selectFields[] = 'wards.WardName';
                    $query .= ' LEFT JOIN wards ON wards.WardId = customers.WardId';
                }
            }
            $query = str_replace('{selectField}', implode(',', $selectFields), $query);
        }
        $query .= " WHERE orders.StatusId > 0" . $this->buildQuery($postData) . ' ORDER BY orders.OrderId DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['OrderCode']) && !empty($postData['OrderCode'])) $query.=" AND orders.OrderCode LIKE '%{$postData['OrderCode']}%'";
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query.=" AND orders.CustomerId = ".$postData['CustomerId'];
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND orders.StatusId = ".$postData['StatusId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND DATE(orders.CrDateTime) >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND DATE(orders.CrDateTime) <= '{$postData['EndDate']}'";

        if(isset($postData['ProductId']) && $postData['ProductId'] > 0) $query.=" AND orderproducts.ProductId = ".$postData['ProductId'];
        if(isset($postData['ProductTypeId']) && $postData['ProductTypeId'] > 0) $query.=" AND products.ProductTypeId = ".$postData['ProductTypeId'];

        if(isset($postData['CustomerTypeId']) && $postData['CustomerTypeId'] > 0) $query.=" AND customers.CustomerTypeId = ".$postData['CustomerTypeId'];
        if(isset($postData['ProvinceId']) && $postData['ProvinceId'] > 0) $query.=" AND customers.ProvinceId = ".$postData['ProvinceId'];
        if(isset($postData['DistrictId']) && $postData['DistrictId'] > 0) $query.=" AND customers.DistrictId = ".$postData['DistrictId'];
        return $query;
    }

    public function update($postData, $orderId = 0, $products = array(), $productQuantities = array()){
        $isUpdate = $orderId > 0;
        $crUserId = $isUpdate ? $postData['UpdateUserId'] : $postData['CrUserId'];
        $crDateTime = $isUpdate ? $postData['UpdateDateTime'] : $postData['CrDateTime'];
        $this->db->trans_begin();
        $orderId = $this->save($postData, $orderId, array('UpdateUserId', 'UpdateDateTime'));
        if(!$isUpdate) $this->save(array('OrderCode' => $this->genOrderCode($orderId, $postData['OrderDate'])), $orderId);
        else $this->db->update('orderproducts', array('StatusId' => 0, 'UpdateUserId' => $crUserId, 'UpdateDateTime' => $crDateTime), array('OrderId' => $orderId));
        if(!empty($products)){
            $orderProductInserts = array();
            foreach($products as $p){
                $orderProductInserts[] = array(
                    'OrderId' => $orderId,
                    'ProductId' => $p['ProductId'],
                    'SellQuantity' => $p['SellQuantity'],
                    'ReturnQuantity' => $p['ReturnQuantity'],
                    'StatusId' => STATUS_ACTIVED,
                    'CrUserId' => $crUserId,
                    'CrDateTime' => $crDateTime
                );
            }
            $this->db->insert_batch('orderproducts', $orderProductInserts);
            foreach($products as $p){
                $flag = false;
                if($p['SellQuantity'] > 0){
                    $importId = $this->Mimports->insert(array(
                        'ImportDate' => $crDateTime,
                        'ProductId' => $p['ProductId'],
                        'OldQuantity' => $productQuantities[$p['ProductId']],
                        'Quantity' => $p['SellQuantity'],
                        'ImportTypeId' => 2,
                        'OrderId' => $orderId,
                        'Comment' => '',
                        'CrUserId' => $crUserId,
                        'CrDateTime' => $crDateTime
                    ));
                    $flag = $importId > 0;
                }
                if($p['ReturnQuantity'] > 0){
                    $this->Mimports->insert(array(
                        'ImportDate' => $crDateTime,
                        'ProductId' => $p['ProductId'],
                        'OldQuantity' => $flag ? $this->Mproducts->getFieldValue(array('ProductId' => $p['ProductId']), 'Quantity', 0) : $productQuantities[$p['ProductId']],
                        'Quantity' => $p['ReturnQuantity'],
                        'ImportTypeId' => 3,
                        'OrderId' => $orderId,
                        'Comment' => '',
                        'CrUserId' => $crUserId,
                        'CrDateTime' => $crDateTime
                    ));
                }
            }
        }
        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $orderId;
        }
    }

    public function genOrderCode($orderId, $orderDate){
        return ddMMyyyy($orderDate, 'dmY') . '-' . (10000 + $orderId);
    }
}