<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mwards extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "wards";
        $this->_primary_key = "WardId";
    }

    public function getList($districtId = 0){
        if($districtId > 0) return $this->getBy(array('DistrictId' => $districtId), false, "DisplayOrder", 'WardId, WardName', 0, 0, 'asc');
        return $this->get(0, false, "DisplayOrder", '', 0, 0, 'asc');
    }

    public function selectHtml($wardId = 0, $selectName = 'WardId', $txtAll = '--Chọn--'){
        $retVal = '<select class="form-control" name="'.$selectName.'" id="'.lcfirst($selectName).'" data-id="'.$wardId.'"><option value="0" data-id="0">'.$txtAll.'</option>';
        $retVal .= '</select>';
        return $retVal;
    }
}