<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcustomers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "customers";
        $this->_primary_key = "CustomerId";
    }

    public function checkExist($customerId, $phoneNumber, $email){
        $query = "SELECT CustomerId FROM customers WHERE CustomerId != ? AND StatusId = ?";
        $param = array($customerId, STATUS_ACTIVED);
        $flag = true;
        if(!empty($phoneNumber) && !empty($email)){
            $query .= " AND (PhoneNumber = ? OR Email = ?) LIMIT 1";
            $param[] = $phoneNumber;
            $param[] = $email;
        }
        elseif(!empty($phoneNumber)){
            $query .= " AND PhoneNumber = ? LIMIT 1";
            $param[] = $phoneNumber;
        }
        elseif(!empty($email)){
            $query .= " AND Email = ? LIMIT 1";
            $param[] = $email;
        }
        else $flag = false;
        if($flag){
            $customers = $this->getByQuery($query, $param);
            if(!empty($customers)) return true;
        }
        return false;
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1, $select = '*', $joins = array()){
        if(empty($joins)) $query = "SELECT {$select} FROM customers";
        else{
            $selectFields = array();
            $query = 'SELECT customers.*,{selectField} FROM customers';
            foreach($joins as $join){
                if($join == 'wards'){
                    $selectFields[] = 'wards.WardName';
                    $query .= ' LEFT JOIN wards ON customers.WardId = wards.WardId';
                }
            }
            $query = str_replace('{selectField}', implode(',', $selectFields), $query);
        }
        $query .= " WHERE StatusId > 0" . $this->buildQuery($postData) . ' ORDER BY customers.CustomerId DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['FullName']) && !empty($postData['FullName'])) $query.=" AND FullName LIKE '%{$postData['FullName']}%'";
        if(isset($postData['PhoneNumber']) && !empty($postData['PhoneNumber'])) $query.=" AND PhoneNumber LIKE '%{$postData['PhoneNumber']}%'";
        if(isset($postData['Email']) && !empty($postData['Email'])) $query.=" AND Email LIKE '%{$postData['Email']}%'";
        if(isset($postData['Address']) && !empty($postData['Address'])) $query.=" AND Address LIKE '%{$postData['Address']}%'";
        if(isset($postData['GenderId']) && $postData['GenderId'] > 0) $query.=" AND GenderId = ".$postData['GenderId'];
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND StatusId = ".$postData['StatusId'];
        if(isset($postData['ProvinceId']) && $postData['ProvinceId'] > 0) $query.=" AND ProvinceId = ".$postData['ProvinceId'];
        if(isset($postData['DistrictId']) && $postData['DistrictId'] > 0) $query.=" AND DistrictId = ".$postData['DistrictId'];
        if(isset($postData['WardId']) && $postData['WardId'] > 0) $query.=" AND WardId = ".$postData['WardId'];
        if(isset($postData['CareStaffId']) && $postData['CareStaffId'] > 0) $query.=" AND CareStaffId = ".$postData['CareStaffId'];
        if(isset($postData['CustomerTypeId']) && $postData['CustomerTypeId'] > 0) $query.=" AND CustomerTypeId = ".$postData['CustomerTypeId'];
        if(isset($postData['OrderTimeTypeId']) && $postData['OrderTimeTypeId'] > 0) $query.=" AND OrderTimeTypeId = ".$postData['OrderTimeTypeId'];
        if(isset($postData['StoreReviewTypeId']) && $postData['StoreReviewTypeId'] > 0) $query.=" AND StoreReviewTypeId = ".$postData['StoreReviewTypeId'];
        if(isset($postData['IntimacyTypeId']) && $postData['IntimacyTypeId'] > 0) $query.=" AND IntimacyTypeId = ".$postData['IntimacyTypeId'];
        /*if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND DATE(CrDateTime) >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND DATE(CrDateTime) <= '{$postData['EndDate']}'";*/
        return $query;
    }

    public function update($postData, $customerId, $provinceCode){
        $isInsert = $customerId == 0;
        $this->db->trans_begin();
        $customerId = $this->save($postData, $customerId, array('BirthDay', 'UpdateUserId', 'UpdateDateTime'));
        if($isInsert) $this->save(array('CustomerCode' => $this->genCustomerCode($customerId, $provinceCode)), $customerId);
        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $customerId;
        }
    }

    public function genCustomerCode($customerId, $provinceCode){
        return $provinceCode . '-' . (10000 + $customerId);
    }

    public function selectHtml($listCustomers, $selectName, $objId = 0, $isAll = false, $txtAll = "", $isShowPhone = true){
        $id = str_replace('[]', '', lcfirst($selectName));
        echo '<select class="form-control select2" name="'.$selectName.'" id="'.$id.'">';
        if($isAll){
            if(empty($txtAll)) echo '<option value="0">Tất cả</option>';
            else echo '<option value="0">'.$txtAll.'</option>';
        }
        $isSelectMultiple = is_array($objId);
        foreach($listCustomers as $obj){
            $selected = '';
            if(!$isSelectMultiple) {
                if($obj['CustomerId'] == $objId) $selected = ' selected="selected"';
            }
            elseif(in_array($obj['CustomerId'], $objId)) $selected = ' selected="selected"';
            if($isShowPhone && !empty($obj['PhoneNumber'])) echo '<option value="'.$obj['CustomerId'].'"'.$selected.'>'.$obj['FullName'].' - '.$obj['PhoneNumber'].'</option>';
            else echo '<option value="'.$obj['CustomerId'].'"'.$selected.'>'.$obj['FullName'].'</option>';
        }
        echo '</select>';
    }
}