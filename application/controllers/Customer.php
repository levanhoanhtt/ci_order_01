<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách Khách hàng',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_date.js', 'js/customer_list.js'))
            )
		);
		if($this->Mactions->checkAccess($data['listActions'], 'customer')) {
            $this->loadModel(array('Mprovinces', 'Mdistricts', 'Mwards', 'Mcustomers'));
            $data['listStaffs'] = $this->Musers->getListByRole(0, true);
			$data['listProvinces'] = $this->Mprovinces->getList();
            $data['listDistricts'] = $this->Mdistricts->getList();
			$postData = $this->arrayFromPost(array('FullName', 'PhoneNumber', 'Email', 'ProvinceId', 'DistrictId', 'WardId', 'Address', 'CareStaffId', 'CustomerTypeId', 'DayCompareTypeId', 'BeginDate', 'EndDate', 'OrderTimeTypeId', 'StoreReviewTypeId', 'IntimacyTypeId'));
            $postData = $this->formatDateSearch($postData);
			$rowCount = $this->Mcustomers->getCount($postData);
			$data['listCustomers'] = array();
			if($rowCount > 0){
				$pageCount = ceil($rowCount / DEFAULT_LIMIT);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listCustomers'] = $this->Mcustomers->search($postData, DEFAULT_LIMIT, $page, '*', array('wards'));
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('customer/list', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function edit($customerId = 0){
		if($customerId > 0) {
			$user = $this->checkUserLogin();
			$data = $this->commonData($user,
				'Cập nhật Khách hàng',
				array(
                    'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/customer_update.js'))
                )
			);
            if($this->Mactions->checkAccess($data['listActions'], 'customer/edit')) {
                $this->loadModel(array('Mprovinces', 'Mdistricts', 'Mwards', 'Mcustomers'));
                $customer = $this->Mcustomers->get($customerId);
                if($customer){
                    $data['customerId'] = $customerId;
                    $data['customer'] = $customer;
                    $data['listStaffs'] = $this->Musers->getListByRole(0, true);
                    $data['listProvinces'] = $this->Mprovinces->getList();
                }
                else {
                    $data['customerId'] = 0;
                    $data['txtError'] = "Không tìm thấy Khách hàng";
                }
                $this->load->view('customer/edit', $data);
            }
            else $this->load->view('user/permission', $data);
		}
		else redirect('user/profile');
	}

	public function add(){
		$user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm khách hàng',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/customer_update.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'customer/add')) {
            $this->loadModel(array('Mprovinces', 'Mdistricts', 'Mwards'));
            $data['listStaffs'] = $this->Musers->getListByRole(0, true);
            $data['listProvinces'] = $this->Mprovinces->getList();
            $this->load->view('customer/add', $data);
        }
        else $this->load->view('user/permission', $data);
	}

	public function update(){
		$user = $this->session->userdata('user');
		$postData = $this->arrayFromPost(array('FullName', 'PhoneNumber', 'Email', 'GenderId', 'StatusId', 'ProvinceId', 'DistrictId', 'WardId', 'Address', 'CareStaffId', 'Comment', 'BirthDay', 'CustomerTypeId', 'OrderTimeTypeId', 'StoreReviewTypeId', 'IntimacyTypeId'));
		if(!empty($postData['FullName']) && $postData['GenderId'] > 0  && $postData['StatusId'] > 0 && $postData['ProvinceId'] > 0 && $postData['CustomerTypeId'] > 0 && $postData['OrderTimeTypeId'] > 0 && $postData['StoreReviewTypeId'] > 0 && $postData['IntimacyTypeId'] > 0){
            $this->loadModel(array('Mcustomers', 'Mprovinces'));
            $customerId = $this->input->post('CustomerId');
			if ($this->Mcustomers->checkExist($customerId, $postData['PhoneNumber'], $postData['Email'])){
				echo json_encode(array('code' => -1, 'message' => "Số điện thoại hoặc Email đã tồn tại trong hệ thống"));
			}
			else {
                $postData['BirthDay'] = ddMMyyyyToDate($postData['BirthDay']);
                $provinceCode = $this->Mprovinces->getFieldValue(array('ProvinceId' => $postData['ProvinceId']), 'ProvinceCode', 'HMD');
				if($customerId > 0){
				    $postData['CustomerCode'] = $this->Mcustomers->genCustomerCode($customerId, $provinceCode);
                    $postData['UpdateUserId'] = $user['UserId'];
                    $postData['UpdateDateTime'] = getCurentDateTime();
                }
				else {
                    $postData['CrUserId'] = $user['UserId'];
                    $postData['CrDateTime'] = getCurentDateTime();
				}
				$customerId = $this->Mcustomers->update($postData, $customerId, $provinceCode);
				if($customerId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật Khách hàng thành công", 'data' => $customerId));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $customerId = $this->input->post('CustomerId');
        $statusId = $this->input->post('StatusId');
        if($customerId > 0 && $statusId >= 0 && $statusId <= count($this->Mconstants->status)) {
            $this->load->model('Mcustomers');
            $flag = $this->Mcustomers->changeStatus($statusId, $customerId, '', $user['UserId']);
            if($flag) {
                $statusName = "";
                if($statusId == 0) $txtSuccess = "Xóa khách hàng thành công";
                else{
                    $txtSuccess = "Đổi trạng thái thành công";
                    $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getInfo(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('CustomerId', 'CustomerTypeId', 'ProvinceId', 'DistrictId', 'WardId', 'StoreReviewTypeId', 'IntimacyTypeId'));
        if($postData['CustomerId'] > 0) {
            $this->loadModel(array('Mprovinces', 'Mdistricts', 'Mwards'));
            $data = array(
                'CustomerTypeName' => $postData['CustomerTypeId'] > 0 ? $this->Mconstants->customerTypes[$postData['CustomerTypeId']] : '',
                'StoreReviewTypeName' => $postData['StoreReviewTypeId'] > 0 ? $this->Mconstants->reviewTypes[$postData['StoreReviewTypeId']] : '',
                'IntimacyTypeName' => $postData['IntimacyTypeId'] > 0 ? $this->Mconstants->reviewTypes[$postData['IntimacyTypeId']] : '',
                'ProvinceName' => $postData['ProvinceId'] > 0 ? $this->Mprovinces->getFieldValue(array('ProvinceId' => $postData['ProvinceId']), 'ProvinceName') : '',
                'DistrictName' => $postData['DistrictId'] > 0 ? $this->Mdistricts->getFieldValue(array('DistrictId' => $postData['DistrictId']), 'DistrictName') : '',
                'WardName' => $postData['WardId'] > 0 ? $this->Mwards->getFieldValue(array('WardId' => $postData['WardId']), 'WardName') : ''
            );
            echo json_encode(array('code' => 1, 'data' => $data));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}