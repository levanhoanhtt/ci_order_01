<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách Sản phẩm',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/product_list.js'))
            )
		);
		if($this->Mactions->checkAccess($data['listActions'], 'product')) {
            $this->loadModel(array('Mproducttypes', 'Munits', 'Mproducts'));
            $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listUnits'] = $this->Munits->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listAllProducts'] = $this->Mproducts->getBy(array('StatusId' => STATUS_ACTIVED));
			$postData = $this->arrayFromPost(array('ProductCode', 'ProductName', 'ProductTypeId', 'UnitId'));
			$rowCount = $this->Mproducts->getCount($postData);
			$data['listProducts'] = array();
			if($rowCount > 0){
				$pageCount = ceil($rowCount / DEFAULT_LIMIT);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listProducts'] = $this->Mproducts->search($postData, DEFAULT_LIMIT, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('product/list', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$user = $this->session->userdata('user');
        $postData = $this->arrayFromPost(array('ProductName', 'ProductTypeId', 'UnitId', 'Quantity', 'StatusId'));
        $postData['Quantity'] = replacePrice($postData['Quantity']);
		if(!empty($postData['ProductName']) && $postData['ProductTypeId'] > 0  && $postData['StatusId'] > 0 && $postData['UnitId'] > 0 && $postData['Quantity'] >= 0){
            $this->load->model('Mproducts');
            $productId = $this->input->post('ProductId');
			if ($this->Mproducts->checkExist($productId, $postData['ProductName'], $postData['ProductTypeId'])){
				echo json_encode(array('code' => -1, 'message' => "Sản phẩm đã tồn tại trong hệ thống"));
			}
			else {
				if($productId > 0){
                    $postData['UpdateUserId'] = $user['UserId'];
                    $postData['UpdateDateTime'] = getCurentDateTime();
                }
				else {
                    $postData['CrUserId'] = $user['UserId'];
                    $postData['CrDateTime'] = getCurentDateTime();
				}
				$productId = $this->Mproducts->update($postData, $productId);
				if($productId > 0){
				    $postData['ProductId'] = $productId;
				    $postData['ProductCode'] = $this->Mproducts->genProductCode($productId);
				    echo json_encode(array('code' => 1, 'message' => "Cập nhật Sản phẩm thành công", 'data' => $postData));
                }
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $productId = $this->input->post('ProductId');
        $statusId = $this->input->post('StatusId');
        if($productId > 0 && $statusId >= 0 && $statusId <= count($this->Mconstants->status)) {
            $this->load->model('Mproducts');
            $flag = $this->Mproducts->changeStatus($statusId, $productId, '', $user['UserId']);
            if($flag) {
                $statusName = "";
                if($statusId == 0) $txtSuccess = "Xóa sản phẩm thành công";
                else{
                    $txtSuccess = "Đổi trạng thái thành công";
                    $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function import(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductId', 'Quantity', 'ImportDate', 'ImportTypeId', 'OrderId', 'Comment'));
        $postData['Quantity'] = replacePrice($postData['Quantity']);
        if($postData['ProductId'] > 0 && $postData['Quantity'] > 0 && $postData['ImportTypeId'] > 0){
            $crDateTime = getCurentDateTime();
            if(!empty($postData['ImportDate'])) $postData['ImportDate'] = ddMMyyyyToDate($postData['ImportDate']);
            else $postData['ImportDate'] = $crDateTime;
            $this->loadModel(array('Mimports', 'Mproducts'));
            $postData['OldQuantity'] = $this->Mproducts->getFieldValue(array('ProductId' => $postData['ProductId']), 'Quantity', 0);
            $postData['CrUserId'] = $user['UserId'];
            $postData['CrDateTime'] = $crDateTime;
            $importId = $this->Mimports->insert($postData);
            if($importId > 0){
                $quantity = $postData['OldQuantity'];
                if($postData['ImportTypeId'] == 2) $quantity -= $postData['Quantity'];
                else $quantity += $postData['Quantity'];
                echo json_encode(array('code' => 1, 'message' => "Nhập hàng vào kho thành công", 'data' => $quantity));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getSellQuantity(){
        $this->checkUserLogin(true);
        $productIds = trim($this->input->post('ProductIds'));
        if(!empty($productIds)){
            $productIds = json_decode($productIds, true);
            $this->load->model('Mproducts');
            $data = $this->Mproducts->getSellQuantity($productIds);
            echo json_encode(array('code' => 1, 'data' => $data));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}