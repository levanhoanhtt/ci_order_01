<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producttype extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Loại sản phẩm',
			array('scriptFooter' => array('js' => 'js/product_type.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'producttype')) {
			$this->load->model('Mproducttypes');
			$data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('setting/product_type', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('ProductTypeName'));
		if(!empty($postData['ProductTypeName'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$productTypeId = $this->input->post('ProductTypeId');
			$this->load->model('Mproducttypes');
			$flag = $this->Mproducttypes->save($postData, $productTypeId);
			if ($flag > 0) {
				$postData['ProductTypeId'] = $flag;
				$postData['IsAdd'] = $productTypeId > 0 ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật Loại sản phẩm thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$this->checkUserLogin(true);
		$productTypeId = $this->input->post('ProductTypeId');
		if($productTypeId > 0){
			$this->load->model('Mproducttypes');
			$flag = $this->Mproducttypes->changeStatus(0, $productTypeId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa loại sản phẩm thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
