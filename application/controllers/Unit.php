<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Đơn vị tính',
			array('scriptFooter' => array('js' => 'js/unit.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'unit')) {
			$this->load->model('Munits');
			$data['listUnits'] = $this->Munits->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('setting/unit', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('UnitName'));
		if(!empty($postData['UnitName'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$unitId = $this->input->post('UnitId');
			$this->load->model('Munits');
			$flag = $this->Munits->save($postData, $unitId);
			if ($flag > 0) {
				$postData['UnitId'] = $flag;
				$postData['IsAdd'] = $unitId > 0 ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật Đơn vị tính thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$this->checkUserLogin(true);
		$unitId = $this->input->post('UnitId');
		if($unitId > 0){
			$this->load->model('Munits');
			$flag = $this->Munits->changeStatus(0, $unitId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa đơn vị tính thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
