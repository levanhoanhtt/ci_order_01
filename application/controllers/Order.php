<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Quản lý hàng bán',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_date.js', 'js/order_list.js'))
            )
		);
		if($this->Mactions->checkAccess($data['listActions'], 'order')) {
            $this->loadModel(array('Mcustomers', 'Mproducts', 'Mproducttypes', 'Munits', 'Mprovinces', 'Mdistricts', 'Mwards', 'Morders'));
            $whereStatus = array('StatusId' => STATUS_ACTIVED);
            $data['listCustomers'] = $this->Mcustomers->search($whereStatus, 0, 1, 'CustomerId, FullName, PhoneNumber');
            $data['listProducts'] = $this->Mproducts->getBy($whereStatus);
            $data['listProductTypes'] = $this->Mproducttypes->getBy($whereStatus);
            $data['listUnits'] = $this->Munits->getBy($whereStatus);
            $data['listProvinces'] = $this->Mprovinces->getList();
            $data['listDistricts'] = $this->Mdistricts->getList();
			$postData = $this->arrayFromPost(array('OrderCode', 'CustomerId', 'CustomerTypeId', 'ProductId', 'ProductTypeId', 'ProvinceId', 'DistrictId', 'DayCompareTypeId', 'BeginDate', 'EndDate'));
            $postData = $this->formatDateSearch($postData);
			$rowCount = $this->Morders->getCount($postData);
			$data['listOrders'] = array();
			if($rowCount > 0){
				$pageCount = ceil($rowCount / DEFAULT_LIMIT);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listOrders'] = $this->Morders->search($postData, DEFAULT_LIMIT, $page, array('orderproducts', 'products', 'customers', 'wards'));
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('order/list', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Nhập hàng ngày',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/order_update.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'order/add')) {
            $this->loadModel(array('Mcustomers', 'Mproducts', 'Mproducttypes', 'Munits'));
            $whereStatus = array('StatusId' => STATUS_ACTIVED);
            $data['listCustomers'] = $this->Mcustomers->search($whereStatus);
            $data['listProducts'] = $this->Mproducts->getBy($whereStatus);
            $data['listProductTypes'] = $this->Mproducttypes->getBy($whereStatus);
            $data['listUnits'] = $this->Munits->getBy($whereStatus);
            $this->load->view('order/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($orderId = 0){
        if($orderId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Xem nhập hàng ngày'
                /*array(
                    'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/customer_update.js'))
                )*/
            );
            if($this->Mactions->checkAccess($data['listActions'], 'order/edit')) {
                $this->loadModel(array('Mprovinces', 'Mdistricts', 'Mwards', 'Mcustomers', 'Morders', 'Mproducts'));
                $order = $this->Morders->get($orderId);
                if($order){
                    $data['title'] .= ' '.$order['OrderCode'];
                    $data['orderId'] = $orderId;
                    $data['order'] = $order;
                    $data['customer'] = $this->Mcustomers->get($order['CustomerId']);
                    $data['listOrderProducts'] = $this->Mproducts->getByOrderId($orderId);
                }
                else {
                    $data['orderId'] = 0;
                    $data['txtError'] = "Không tìm thấy Nhập hàng ngày";
                }
                $this->load->view('order/edit', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('user/profile');
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('CustomerId', 'StatusId', 'OrderDate'));
        $products = $this->input->post('Products');
        if($postData['CustomerId'] > 0 && $postData['StatusId'] > 0 && !empty($products)){
            $this->loadModel(array('Morders', 'Mproducts', 'Mimports'));
            $productIds = array();
            foreach($products as $p) $productIds[] = $p['ProductId'];
            $productQuantities = $this->Mproducts->getQuantityByIds($productIds);
            foreach($products as $p){
                if($p['SellQuantity'] > $productQuantities[$p['ProductId']]){
                    $productName = $this->Mproducts->getFieldValue(array('ProductId' => $p['ProductId']), 'ProductName');
                    echo json_encode(array('code' => 0, 'message' => "Số lượng sản phẩm {$productName} không đủ để bán"));
                    die();
                }
            }
            $crDateTime = getCurentDateTime();
            if(!empty($postData['OrderDate'])) $postData['OrderDate'] = ddMMyyyyToDate($postData['OrderDate']);
            else $postData['OrderDate'] = $crDateTime;
            $orderId = $this->input->post('OrderId');
            if($orderId > 0){
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = getCurentDateTime();
            }
            else{
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = getCurentDateTime();
            }
            $orderId = $this->Morders->update($postData, $orderId, $products, $productQuantities);
            if($orderId > 0) echo json_encode(array('code' => 1, 'message' => "Nhập hàng ngày thành công", 'data' => $orderId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}