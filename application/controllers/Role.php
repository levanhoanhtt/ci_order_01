<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Cấp quyền phân cấp',
			array('scriptFooter' => array('js' => 'js/role.js'))
		);
		if ($this->Mactions->checkAccess($data['listActions'], 'role')) {
			$data['listActiveActions'] = $this->Mactions->getHierachy();
			$this->load->view('setting/role', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function getAction(){
		$this->checkUserLogin(true);
		$roleId = $this->input->post('RoleId');
		if($roleId > 0){
			$this->load->model('Mroleactions');
			echo json_encode(array('code' => 1, 'data' => $this->Mroleactions->getBy(array('RoleId' => $roleId), false, '', 'ActionId')));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function update(){
		$user = $this->checkUserLogin(true);
		$roleId = $this->input->post('RoleId');
		if ($roleId > 0) {
			$valueData = array();
			$actionIds = $this->input->post('ActionIds');
			if(!empty($actionIds)){
				$crDateTime = getCurentDateTime();
				$actionIds = json_decode($actionIds, true);
				foreach($actionIds as $actionId) $valueData[] = array('RoleId' => $roleId, 'ActionId' => $actionId, 'CrUserId' => $user['UserId'], 'CrDateTime' => $crDateTime);
			}
			$this->load->model('Mroleactions');
			$flag = $this->Mroleactions->updateBatch($roleId, $valueData);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Cấp quyên truy cập thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
