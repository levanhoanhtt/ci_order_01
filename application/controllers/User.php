<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function index(){
		if(!$this->session->userdata('user')){
			$data = array('title' => 'Đăng nhập');
			if ($this->session->flashdata('txtSuccess')) $data['txtSuccess'] = $this->session->flashdata('txtSuccess');
			$this->load->helper('cookie');
			$data['userName'] = $this->input->cookie('userName', true);
			$data['userPass'] = $this->input->cookie('userPass', true);
			$this->load->view('user/login', $data);
		}
		else redirect('user/dashboard');
	}

	public function logout(){
		$user = $this->session->userdata('user');
		$fields = array('user', 'configs');
		foreach($fields as $field) $this->session->unset_userdata($field);
        redirect('admin');
	}

	public function forgotPass(){
		$this->load->view('user/forgot_pass', array('title' => 'Quên mật khẩu'));
	}

	public function sendToken(){
		$email = trim($this->input->post('Email'));
		if(!empty($email)){
			$user = $this->Musers->getBy(array('UserStatusId' => STATUS_ACTIVED, 'Email' => $email), true, "", "UserId,FullName");
			if($user){
				$token = bin2hex(mcrypt_create_iv(10, MCRYPT_DEV_RANDOM));
				$token = substr($token, 0, 14);
				$message = "Xin chào {$user['FullName']}<br/>Xin vào link ".base_url('user/changePass/'.$token).' để đổi mật khẩu.';
				$configs = $this->session->userdata('configs');
				if(!$configs) $configs = array();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : SITE_EMAIL;
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : SITE_NAME;
				$flag = $this->sendMail($emailFrom, $companyName, $email, 'Lấy lại mật khẩu', $message);
				if($flag){
					$this->Musers->save(array('Token' => $token), $user['UserId']);
					$this->load->view('user/forgot_pass', array('title' => 'Quên mật khẩu', 'txtSuccess' => 'Kiểm tra email và làm theo hướng dẫn'));
				}
			}
			else $this->load->view('user/forgot_pass', array('title' => 'Quên mật khẩu', 'txtError' => 'Người dùng không tốn tại hoặc chưa kích hoạt!'));
		}
		else $this->load->view('user/forgot_pass', array('title' => 'Quên mật khẩu', 'txtError' => 'Email không được bỏ trống!'));
	}

	public function changePass($token = ''){
		$data = array('title' => 'Đổi mật khẩu', 'token' => $token);
		$isWrongToken = true;
		if(!empty($token)){
			$user = $this->Musers->getBy(array('UserStatusId' => STATUS_ACTIVED, 'Token' => $token), true, "", "UserId");
			if($user){
				if($this->input->post('UserPass')) {
					$postData = $this->arrayFromPost(array('UserPass', 'RePass'));
					if (!empty($postData['UserPass']) && $postData['UserPass'] == $postData['RePass']) {
						$this->Musers->save(array('UserPass' => md5($postData['UserPass']), 'Token' => ''), $user['UserId']);
						$this->session->set_flashdata('txtSuccess', "Đổi mật khẩu thành công");
						redirect('admin');
						exit();
					}
					else $data['txtError'] = "Mật khẩu không trùng";
				}
			}
			else {
				$data['txtError'] = "Mã Token không dúng";
				$isWrongToken = false;
			}
		}
		else {
			$data['txtError'] = "Mã Token không dúng";
			$isWrongToken = false;
		}
		$data['isWrongToken'] = $isWrongToken;
		$this->load->view('user/change_pass', $data);
	}

	public function register(){
		if($this->session->userdata('user')) redirect('user/dashboard');
		else{
			$this->load->model('Mprovinces');
			$data = array(
				'title' => 'Đăng ký tài khoản',
				'listProvinces' => $this->Mprovinces->getList()
			);
			$this->load->view('user/register', $data);
		}
	}

	public function permission(){
		$this->load->view('user/permission');
	}

    public function dashboard(){
        redirect('user/profile');
    }

	public function profile(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Trang cá nhân - '.$user['FullName'],
			array('scriptFooter' =>array('js' => array('js/user_update.js')))
		);
		$this->loadModel(array('Mprovinces', 'Mdistricts', 'Mwards'));
		$data['listProvinces'] = $this->Mprovinces->getList();
		$this->load->view('user/profile', $data);
	}

	public function staff(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách Nhân viên',
			array('scriptFooter' => array('js' => 'js/user_list.js'))
		);
		$listActions = $data['listActions'];
		if($this->Mactions->checkAccess($listActions, 'user/staff')) {
			$this->load->model('Mprovinces');
			$data['listProvinces'] = $this->Mprovinces->getList();
			$postData = $this->arrayFromPost(array('UserName', 'FullName', 'Email', 'PhoneNumber', 'RoleId', 'UserStatusId', 'GenderId', 'ProvinceId'));
			$rowCount = $this->Musers->getCount($postData);
			$data['listUsers'] = array();
			if($rowCount > 0){
				$pageCount = ceil($rowCount / DEFAULT_LIMIT);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listUsers'] = $this->Musers->search($postData, DEFAULT_LIMIT, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('user/staff', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function edit($userId = 0){
		if($userId > 0) {
			$user = $this->checkUserLogin();
			$data = $this->commonData($user,
				'Cập nhật Nhân viên',
				array('scriptFooter' => array('js' => array('js/user_update.js')))
			);
            if($this->Mactions->checkAccess($data['listActions'], 'user/edit')) {
                $userEdit = $this->Musers->get($userId);
                if($userEdit) {
                    $data['userId'] = $userId;
                    $data['userEdit'] = $userEdit;
                    $this->loadModel(array('Mprovinces', 'Mdistricts', 'Mwards'));
                    $data['listProvinces'] = $this->Mprovinces->getList();
                }
                else {
                    $data['userId'] = 0;
                    $data['txtError'] = "Không tìm thấy Nhân viên";
                }
                $this->load->view('user/edit', $data);
            }
            else $this->load->view('user/permission', $data);
		}
		else redirect('user/profile');
	}

	public function add(){
		$user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm nhân viên',
            array('scriptFooter' => array('js' => array('js/user_update.js')))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'user/add')) {
            $this->loadModel(array('Mprovinces', 'Mdistricts', 'Mwards'));
            $data['listProvinces'] = $this->Mprovinces->getList();
            $this->load->view('user/add', $data);
        }
        else $this->load->view('user/permission', $data);
	}

	//api
    public function updateProfile(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('UserName', 'UserPass', 'NewPass', 'FullName', 'PhoneNumber', 'Email', 'GenderId', 'ProvinceId', 'DistrictId', 'WardId', 'Address', 'Avatar'));
        if(!empty($postData['UserName']) && !empty($postData['FullName']) && $postData['GenderId'] > 0) {
            $flag = false;
            if (!empty($postData['NewPass'])) {
                if ($user['UserPass'] == md5($postData['UserPass'])) {
                    $flag = true;
                    $postData['UserPass'] = md5($postData['NewPass']);
                    unset($postData['NewPass']);
                }
                else echo json_encode(array('code' => -1, 'message' => "Mật khẩu cũ không đúng"));
            }
            else {
                $flag = true;
                unset($postData['UserPass']);
                unset($postData['NewPass']);
            }
            if ($flag) {
                if ($this->Musers->checkExist($user['UserId'], $postData['RoleId'], $postData['UserName'], $postData['PhoneNumber'], $postData['Email'])) {
                    echo json_encode(array('code' => -1, 'message' => "Tên đăng nhập hoặc Số điện thoại đã tồn tại trong hệ thống"));
                }
                else {
                    $postData['Avatar'] = replaceFileUrl($postData['Avatar'], USER_PATH);
                    $postData['UpdateUserId'] = $user['UserId'];
                    $postData['UpdateDateTime'] = getCurentDateTime();
                    $flag = $this->Musers->save($postData, $user['UserId'], array('Token'));
                    if ($flag) {
                        $user = array_merge($user, $postData);
                        $this->session->set_userdata('user', $user);
                        echo json_encode(array('code' => 1, 'message' => "Cập nhật thông tin cá nhân thành công"));
                    }
                    else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                }
            }
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

	public function saveUser(){
		$user = $this->session->userdata('user');
		$postData = $this->arrayFromPost(array('UserName', 'UserPass', 'FullName', 'PhoneNumber', 'Email', 'RoleId', 'GenderId', 'UserStatusId', 'ProvinceId', 'DistrictId', 'WardId', 'Address', 'Avatar', 'Comment'));
		$userId = $this->input->post('UserId');
		if(empty($postData['UserName'])) $postData['UserName'] = $postData['PhoneNumber'];
		if(!empty($postData['UserName']) && !empty($postData['FullName']) && $postData['RoleId'] > 0) {
			if ($this->Musers->checkExist($userId, $postData['RoleId'], $postData['UserName'], $postData['PhoneNumber'], $postData['Email'])) {
				echo json_encode(array('code' => -1, 'message' => "Tên đăng nhập hoặc Số điện thoại đã tồn tại trong hệ thống"));
			}
			else {
				$postData['Avatar'] = replaceFileUrl($postData['Avatar'], USER_PATH);
				$userPass = $postData['UserPass'];
				if($userId == 0){
                    $postData['UserPass'] = md5($userPass);
                    $postData['CrUserId'] = ($user) ? $user['UserId'] : 0;
                    $postData['CrDateTime'] = getCurentDateTime();
                }
				else {
					unset($postData['UserPass']);
					$newPass = trim($this->input->post('NewPass'));
					if(!empty($newPass)) $postData['UserPass'] = md5($newPass);
                    $postData['UpdateUserId'] = $user['UserId'];
                    $postData['UpdateDateTime'] = getCurentDateTime();
				}
				$userId = $this->Musers->save($postData, $userId, array('Token', 'UpdateUserId', 'UpdateDateTime'));
				if ($userId > 0) {
					if ($this->input->post('IsSendPass') == 'on') {
						$message = "Xin chào {$postData['FullName']}<br/>Thông tin đăng nhập của bạn là:<br/>Địa chỉ: " . base_url() . "<br/>Tên đăng nhập: {$postData['UserName']}<br/>Mật khẩu: {$userPass}";
						$configs = $this->session->userdata('configs');
						if (!$configs) $configs = array();
						$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : SITE_EMAIL;
						$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : SITE_NAME;
						$this->sendMail($emailFrom, $companyName, $postData['Email'], 'Thông tin đăng nhập', $message);
					}
					echo json_encode(array('code' => 1, 'message' => "Cập nhật Nhân viên thành công", 'data' => $userId));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function checkLogin(){
		$postData = $this->arrayFromPost(array('UserName', 'UserPass', 'IsRemember', 'IsGetConfigs'));
		$userName = $postData['UserName'];
		$userPass = $postData['UserPass'];
		if(!empty($userName) && !empty($userPass)) {
			$user = $this->Musers->login($userName, $userPass);
			if ($user) {

				if (empty($user['Avatar'])) $user['Avatar'] = NO_IMAGE;
				$this->session->set_userdata('user', $user);
				if ($postData['IsGetConfigs'] == 1) {
					$this->load->model('Mconfigs');
					$configs = $this->Mconfigs->getListMap();
					$this->session->set_userdata('configs', $configs);
				}
				if ($postData['IsRemember'] == 'on') {
					$this->load->helper('cookie');
					$this->input->set_cookie(array('name' => 'userName', 'value' => $userName, 'expire' => '86400'));
					$this->input->set_cookie(array('name' => 'userPass', 'value' => $userPass, 'expire' => '86400'));
				}
				echo json_encode(array('code' => 1, 'message' => "Đăng nhập thành công"));
			}
			else echo json_encode(array('code' => 0, 'message' => "Đăng nhập không thành công"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function requestSendToken(){
		$email = trim($this->input->post('Email'));
		if(!empty($email)){
			$user = $this->Musers->getBy(array('UserStatusId' => STATUS_ACTIVED, 'Email' => $email), true, "", "UserId,FullName");
			if($user){
				$token = bin2hex(mcrypt_create_iv(10, MCRYPT_DEV_RANDOM));
				$token = substr($token, 0, 14);
				$message = "Xin chào {$user['FullName']}<br/>Xin vào link ".base_url('user/changePass/'.$token).' để đổi mật khẩu.';
				$configs = $this->session->userdata('configs');
				if(!$configs) $configs = array();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : SITE_EMAIL;
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : SITE_NAME;
				$flag = $this->sendMail($emailFrom, $companyName, $email, 'Lấy lại mật khẩu', $message);
				if($flag){
					$this->Musers->save(array('Token' => $token), $user['UserId']);
					echo json_encode(array('code' => 1, 'message' => "Kiểm tra email và làm theo hướng dẫn"));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => 0, 'message' => "Người dùng không tốn tại hoặc chưa kích hoạt"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Email không được bỏ trống"));
	}

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $userId = $this->input->post('UserId');
        $statusId = $this->input->post('UserStatusId');
        if($userId > 0 && $statusId >= 0 && $statusId <= count($this->Mconstants->userStatus)) {
            $flag = $this->Musers->changeStatus($statusId, $userId, 'UserStatusId', $user['UserId']);
            if($flag) {
                $statusName = "";
                if($statusId == 0) $txtSuccess = "Xóa nhân viên thành công";
                else{
                    $txtSuccess = "Đổi trạng thái thành công";
                    $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->userStatus[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}