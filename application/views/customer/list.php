<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb', array('pageLinks' => array(array('Link' => base_url('customer/add'), 'FontAwesome' => 'fa-plus', 'Name' => 'Thêm khách hàng')))); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('customer'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', set_value('ProvinceId'), true, '--Tỉnh/ Thành phố--', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php echo $this->Mdistricts->selectHtml(0, 'DistrictId', $listDistricts, '--Quận/ Huyện--'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php echo $this->Mwards->selectHtml(0, 'WardId', '--Phường / Xã--'); ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="Address" class="form-control" value="<?php echo set_value('Address'); ?>" placeholder="Địa chỉ">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="FullName" class="form-control" value="<?php echo set_value('FullName'); ?>" placeholder="Họ và tên">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="PhoneNumber" class="form-control" value="<?php echo set_value('PhoneNumber'); ?>" placeholder="Số điện thoại">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="Email" class="form-control" value="<?php echo set_value('Email'); ?>" placeholder="Email">
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Musers->selectHtml($listStaffs, 'CareStaffId', set_value('CareStaffId'), true, '--Nhân viên phụ trách--'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('customerTypes', 'CustomerTypeId', set_value('CustomerTypeId'), true, '--Phân loại--'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('dayCompareTypes', 'DayCompareTypeId', set_value('DayCompareTypeId')); ?>
                            </div>
                            <div class="col-sm-3 divDate" id="divBeginDate">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3 divDate" id="divEndDate">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?php $this->Mconstants->selectConstants('orderTimeTypes', 'OrderTimeTypeId', set_value('OrderTimeTypeId'), true, '--Thời gian bán--'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('reviewTypes', 'StoreReviewTypeId', set_value('StoreReviewTypeId'), true, '--Đánh giá vị trí cửa hàng--'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('reviewTypes', 'IntimacyTypeId', set_value('IntimacyTypeId'), true, '--Mức độ thân thiết--'); ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th class="text-center">Mã KH</th>
                                <th>Tên KH</th>
                                <th>Địa chỉ</th>
                                <th>SĐT</th>
                                <th>Email</th>
                                <th class="text-center">Phân loại KH</th>
                                <th class="text-center">Thời gian bán SP</th>
                                <th class="text-center">Đánh giá vị trí cửa hàng</th>
                                <th class="text-center">Mức độ thân thiết</th>
                                <th>Người phụ trách</th>
                                <th style="width: 50px;"></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyCustomer">
                            <?php $i = 0;
                            $customerTypes = $this->Mconstants->customerTypes;
                            $orderTimeTypes = $this->Mconstants->orderTimeTypes;
                            $reviewTypes = $this->Mconstants->reviewTypes;
                            $labelCss = $this->Mconstants->labelCss;
                            foreach($listCustomers as $c){
                                $i++; ?>
                                <tr id="customer_<?php echo $c['CustomerId']; ?>">
                                    <td class="text-center"><?php echo $i; ?></td>
                                    <td class="text-center"><a href="<?php echo base_url('customer/edit/'.$c['CustomerId']); ?>"><?php echo $c['CustomerCode']; ?></a></td>
                                    <td><?php echo $c['FullName']; ?></td>
                                    <td>
                                        <?php echo $c['Address']; ?><?php echo empty($c['Address']) ? $c['WardName'] : ', '.$c['WardName']; ?>
                                        <?php if($c['DistrictId'] > 0) echo '<br>Quận/ Huyện: '.$this->Mconstants->getObjectValue($listDistricts, 'DistrictId', $c['DistrictId'], 'DistrictName');
                                        echo '<br>Tỉnh/ Thành phố: '.$this->Mconstants->getObjectValue($listProvinces, 'ProvinceId', $c['ProvinceId'], 'ProvinceName'); ?>
                                    </td>
                                    <td><?php echo $c['PhoneNumber']; ?></td>
                                    <td><?php echo $c['Email']; ?></td>
                                    <td class="text-center"><span class="<?php echo $labelCss[$c['CustomerTypeId']]; ?>"><?php echo $customerTypes[$c['CustomerTypeId']]; ?></span></td>
                                    <td class="text-center"><span class="<?php echo $labelCss[$c['OrderTimeTypeId']]; ?>"><?php echo $orderTimeTypes[$c['OrderTimeTypeId']]; ?></span></td>
                                    <td class="text-center"><span class="<?php echo $labelCss[$c['StoreReviewTypeId']]; ?>"><?php echo $reviewTypes[$c['StoreReviewTypeId']]; ?></span></td>
                                    <td class="text-center"><span class="<?php echo $labelCss[$c['IntimacyTypeId']]; ?>"><?php echo $reviewTypes[$c['IntimacyTypeId']]; ?></span></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listStaffs, 'UserId', $c['CareStaffId'], 'FullName'); ?></td>
                                    <td class="actions"><a href="javascript:void(0)" class="link_delete" data-id="<?php echo $c['CustomerId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('customer/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>