<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb', array('pageLinks' => array(array('Link' => base_url('customer'), 'FontAwesome' => 'fa-list', 'Name' => 'Danh sách Khách hàng')))); ?>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($customerId > 0){ ?>
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tabCustomerInfo" data-toggle="tab" aria-expanded="false">Thông tin cơ bản</a></li>
                            <li><a href="#tabCustomerOrder" data-toggle="tab" aria-expanded="false">Danh sách đơn hàng</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabCustomerInfo">
                                <?php echo form_open('customer/update', array('id' => 'customerForm')); ?>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Họ và tên <span class="required">*</span></label>
                                            <input type="text" name="FullName" class="form-control hmdrequired" value="<?php echo $customer['FullName']; ?>" data-field="Họ và tên">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Điện thoại <span class="required">*</span></label>
                                            <input type="text" name="PhoneNumber" id="phoneNumber" class="form-control hmdrequired" value="<?php echo $customer['PhoneNumber']; ?>" data-field="Điện thoại">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Email <span class="required">*</span></label>
                                            <input type="text" name="Email" class="form-control hmdrequired" value="<?php echo $customer['Email']; ?>" data-field="Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Phân loại</label>
                                            <?php $this->Mconstants->selectConstants('customerTypes', 'CustomerTypeId', $customer['CustomerTypeId']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Giới tính</label>
                                            <?php $this->Mconstants->selectConstants('genders', 'GenderId', $customer['GenderId']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Tỉnh/ Thành phố <span class="required">*</span></label>
                                            <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', $customer['ProvinceId'], true, '--Chọn--', ' select2 hmdrequiredNumber', ' data-field="Tỉnh/ Thành phố"'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Quận/ Huyện</label>
                                            <?php echo $this->Mdistricts->selectHtml($customer['DistrictId']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Phường / Xã</label>
                                            <?php echo $this->Mwards->selectHtml($customer['WardId']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Địa chỉ <span class="required">*</span></label>
                                            <input type="text" name="Address" class="form-control hmdrequired" value="<?php echo $customer['Address']; ?>" data-field="Địa chỉ">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Ngày sinh</label>
                                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                                <input type="text" class="form-control datepicker" name="BirthDay" value="<?php echo ddMMyyyy($customer['BirthDay']); ?>" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Nhân viên phụ trách</label>
                                            <?php $this->Musers->selectHtml($listStaffs, 'CareStaffId', $customer['CareStaffId'], true, '--Chọn--', true); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Ghi chú</label>
                                            <input type="text" name="Comment" class="form-control" value="<?php echo $customer['Comment']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Thời gian bán</label>
                                            <div class="radio-group">
                                                <?php foreach($this->Mconstants->orderTimeTypes as $i => $v){ ?>
                                                    <span class="item"><input type="radio" name="OrderTimeTypeId" class="iCheck" value="<?php echo $i; ?>"<?php if($i == $customer['OrderTimeTypeId']) echo ' checked'; ?>> <?php echo $v; ?></span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Đánh giá vị trí cửa hàng</label>
                                            <div class="radio-group">
                                                <?php foreach($this->Mconstants->reviewTypes as $i => $v){ ?>
                                                    <span class="item"><input type="radio" name="StoreReviewTypeId" class="iCheck" value="<?php echo $i; ?>"<?php if($i == $customer['OrderTimeTypeId']) echo ' checked'; ?>> <?php echo $v; ?></span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Mức độ thân thiết</label>
                                            <div class="radio-group">
                                                <?php foreach($this->Mconstants->reviewTypes as $i => $v){ ?>
                                                    <span class="item"><input type="radio" name="IntimacyTypeId" class="iCheck" value="<?php echo $i; ?>"<?php if($i == $customer['OrderTimeTypeId']) echo ' checked'; ?>> <?php echo $v; ?></span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group text-right">
                                    <button class="btn btn-primary" type="button" id="btnSubmit">Cập nhật</button>
                                    <input type="text" name="CustomerId" id="customerId" hidden="hidden" value="<?php echo $customerId; ?>">
                                    <input type="text" name="StatusId" hidden="hidden" value="<?php echo $customer['StatusId']; ?>">
                                    <input type="text" id="customerEditUrl" hidden="hidden" value="<?php echo base_url('customer/edit'); ?>/">
                                </div>
                                <?php echo form_close(); ?>                                
                            </div>
                            <div class="tab-pane" id="tabCustomerOrder">
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center">STT</th>
                                            <th class="text-center">Mã đơn hàng</th>
                                            <th>Tên sẩn phẩm</th>
                                            <th class="text-center">Số lượng hàng lấy</th>
                                            <th class="text-center">Ngày lấy hàng</th>
                                            <th class="text-center">Số lượng hoàn trả</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>                            
                        </div>
                    </div>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>