<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Loại sản phẩm</th>
                                <th style="width: 50px;"></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProductType">
                            <?php
                            foreach($listProductTypes as $pt){ ?>
                                <tr id="productType_<?php echo $pt['ProductTypeId']; ?>">
                                    <td id="productTypeName_<?php echo $pt['ProductTypeId']; ?>"><?php echo $pt['ProductTypeName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $pt['ProductTypeId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $pt['ProductTypeId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php echo form_open('producttype/update', array('id' => 'productTypeForm')); ?>
                                <td><input type="text" class="form-control hmdrequired" id="productTypeName" name="ProductTypeName" value="" data-field="Loại sản phẩm"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="ProductTypeId" id="productTypeId" value="0" hidden="hidden">
                                    <input type="text" id="deleteProductTypeUrl" value="<?php echo base_url('producttype/delete'); ?>" hidden="hidden">
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
