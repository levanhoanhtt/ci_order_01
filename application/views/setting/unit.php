<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Đơn vị tính</th>
                                <th style="width: 50px;"></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyUnit">
                            <?php
                            foreach($listUnits as $u){ ?>
                                <tr id="unit_<?php echo $u['UnitId']; ?>">
                                    <td id="unitName_<?php echo $u['UnitId']; ?>"><?php echo $u['UnitName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $u['UnitId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $u['UnitId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php echo form_open('unit/update', array('id' => 'unitForm')); ?>
                                <td><input type="text" class="form-control hmdrequired" id="unitName" name="UnitName" value="" data-field="Đơn vị tính"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="UnitId" id="unitId" value="0" hidden="hidden">
                                    <input type="text" id="deleteUnitUrl" value="<?php echo base_url('unit/delete'); ?>" hidden="hidden">
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>