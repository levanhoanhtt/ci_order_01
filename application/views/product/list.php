<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb', array('pageLinks' => array(
                    array('Link' => 'javascript:void(0)', 'FontAwesome' => 'fa-plus', 'Name' => 'Thêm sản phẩm', 'id' => 'aAddProduct'),
                    array('Link' => 'javascript:void(0)', 'FontAwesome' => 'fa-plus', 'Name' => 'Nhập hàng', 'id' => 'aAddImport')
            ))); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('product'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="ProductCode" class="form-control" value="<?php echo set_value('ProductCode'); ?>" placeholder="Mã sản phẩm">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="ProductName" class="form-control" value="<?php echo set_value('ProductName'); ?>" placeholder="Tên sản phẩm">
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listProductTypes, 'ProductTypeId', 'ProductTypeName', 'ProductTypeId', set_value('ProductTypeId'), true, '--Loại sản phẩm--', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listUnits, 'UnitId', 'UnitName', 'UnitId', set_value('UnitId'), true, '--Đơn vị tính--', ' select2'); ?>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : '');
                    $productIds = array(); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">Mã sản phẩm</th>
                                <th>Tên sản phẩm</th>
                                <th>Loại sản phẩm</th>
                                <th>Đơn vị tính</th>
                                <th class="text-center">Số lượng tồn kho</th>
                                <th class="text-center">Số lượng đã bán</th>
                                <th style="width: 52px;"></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProduct">
                            <?php foreach($listProducts as $p){
                                $productIds[] = intval($p['ProductId']); ?>
                                <tr id="product_<?php echo $p['ProductId']; ?>">
                                    <td class="text-center"><a href="javascript:void(0)" class="aProductCode" data-id="<?php echo $p['ProductId']; ?>"><?php echo $p['ProductCode']; ?></a></td>
                                    <td class="tdProductName"><?php echo $p['ProductName']; ?></td>
                                    <td class="tdProductTypeName"><?php echo $this->Mconstants->getObjectValue($listProductTypes, 'ProductTypeId', $p['ProductTypeId'], 'ProductTypeName'); ?></td>
                                    <td class="tdUnitName"><?php echo $this->Mconstants->getObjectValue($listUnits, 'UnitId', $p['UnitId'], 'UnitName'); ?></td>
                                    <td class="text-center tdQuantity"><?php echo priceFormat($p['Quantity']); ?></td>
                                    <td class="text-center tdSellQuantity"><i class="fa fa-spinner"></i></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $p['ProductId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <a href="javascript:void(0)" class="link_import" data-id="<?php echo $p['ProductId']; ?>" title="Nhập hàng"><i class="fa fa-product-hunt"></i></a>
                                        <input type="text" hidden="hidden" class="productTypeId" value="<?php echo $p['ProductTypeId']; ?>">
                                        <input type="text" hidden="hidden" class="unitId" value="<?php echo $p['UnitId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('product/changeStatus'); ?>">
                    <input type="text" hidden="hidden" id="getSellQuantityUrl" value="<?php echo base_url('product/getSellQuantity'); ?>">
                    <input type="text" hidden="hidden" id="productIds" value="<?php echo json_encode($productIds); ?>">
                    <div class="modal fade" id="modalProduct" tabindex="-1" role="dialog" aria-labelledby="modalProduct">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Cập nhật sản phẩm</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Tên sản phẩm <span class="required">*</span></label>
                                                <input type="text" id="productName" class="form-control hmdrequired" value="" data-field="Tên sản phẩm">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Loại sản phẩm</label>
                                                <?php $this->Mconstants->selectObject($listProductTypes, 'ProductTypeId', 'ProductTypeName', 'ProductTypeId1', 0, true, '--Loại sản phẩm--', ' select2 hmdrequiredNumber', ' data-field="Loại sản phẩm"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Số lượng</label>
                                                <input type="text" id="quantity" class="form-control" value="0">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Đơn vị tính</label>
                                                <?php $this->Mconstants->selectObject($listUnits, 'UnitId', 'UnitName', 'UnitId1', 0, true, '--Đơn vị tính--', ' select2 hmdrequiredNumber', ' data-field="Đơn vị tính"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                                    <button type="button" class="btn btn-primary" id="btnUpdateProduct">Cập nhật</button>
                                    <input type="text" hidden="hidden" id="productId" value="0">
                                    <input type="text" hidden="hidden" id="statusId" value="<?php echo STATUS_ACTIVED; ?>">
                                    <input type="text" hidden="hidden" id="updateProductUrl" value="<?php echo base_url('product/update'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="modalImport">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Nhập hàng vào kho</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label class="control-label">Sản phẩm <span class="required">*</span></label>
                                        <select class="form-control select2" id="importProductId">
                                            <option value="0">--Chọn--</option>
                                            <?php foreach($listAllProducts as $p){ ?>
                                                <option value="<?php echo $p['ProductId']; ?>" data-id="<?php echo $p['UnitId']; ?>"><?php echo $p['ProductCode'] . ' - ' . $p['ProductName'] . ' - ' .$this->Mconstants->getObjectValue($listProductTypes, 'ProductTypeId', $p['ProductTypeId'], 'ProductTypeName') ; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Ngày nhập hàng</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <input type="text" class="form-control datepicker" id="importDate" value="<?php echo date('d/m/Y'); ?>" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Số lượng</label>
                                                <div class="input-group">
                                                    <input type="text" id="quantityPlus" class="form-control" value="0">
                                                    <span class="input-group-addon" id="spanUnitName"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Ghi chú</label>
                                        <input type="text" class="form-control" id="comment" value="">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                                    <button type="button" class="btn btn-primary" id="btnInsertImport">Cập nhật</button>
                                    <input type="text" hidden="hidden" id="insertImportUrl" value="<?php echo base_url('product/import'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>