<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url();?>"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php $this->load->view('includes/favicon'); ?>
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/vendor/plugins/pnotify/pnotify.custom.css" />
    <link rel="stylesheet" href="assets/vendor/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="assets/vendor/plugins/iCheck/square/blue.css">
    <?php if(isset($scriptHeader)) outputScript($scriptHeader); ?>
    <link rel="stylesheet" href="assets/vendor/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="assets/vendor/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="assets/vendor/plugins/pace/pace.min.css">
    <link rel="stylesheet" href="assets/vendor/dist/css/style.css?20180403">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="<?php echo base_url('user/dashboard'); ?>" class="navbar-brand"><b>QL</b> Bán hàng</a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <?php foreach($listActions as $pAction) {
                            if ($pAction['ActionLevel'] == 1 && $pAction['DisplayOrder'] > 0) {
                                $hasChild = false;
                                if (empty($pAction['ActionUrl'])) $hasChild = true;
                                if($hasChild){ ?>
                                    <li class="dropdown">
                                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><?php echo $pAction['ActionName']; ?> <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <?php foreach($listActions as $cAction){
                                                if($cAction['ActionLevel'] == 2 && $cAction['DisplayOrder'] > 0 && $cAction['ParentActionId'] == $pAction['ActionId']){ ?>
                                                    <li><a href="<?php echo base_url($cAction['ActionUrl']); ?>"><?php echo $cAction['ActionName']; ?></a></li>
                                                <?php }
                                            } ?>
                                        </ul>
                                    </li>
                                <?php } else{ ?>
                                    <li><a href="<?php echo base_url($pAction['ActionUrl']); ?>"><?php echo $pAction['ActionName']; ?></a></li>
                                <?php }
                            }
                        } ?>
                    </ul>
                </div>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <?php $avatar = USER_PATH.((!empty($user['Avatar'])) ? $user['Avatar'] : NO_IMAGE); ?>
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo $avatar; ?>" alt="<?php echo $user['FullName']; ?>" class="user-image">
                                <span class="hidden-xs"><?php echo $user['FullName']; ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header">
                                    <img src="<?php echo $avatar; ?>" alt="<?php echo $user['FullName']; ?>" class="img-circle">
                                    <p>
                                        <?php echo $user['FullName']; ?>
                                        <small><?php echo $this->Mconstants->roles[$user['RoleId']]; ?></small>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php echo site_url('user/profile'); ?>" class="btn btn-default btn-flat">Trang cá nhân</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo site_url('user/logout'); ?>" class="btn btn-default btn-flat">Đăng xuất</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>