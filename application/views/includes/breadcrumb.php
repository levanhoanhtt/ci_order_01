<section class="content-header">
    <h1><?php echo $title; ?></h1>
    <ol class="breadcrumb">
        <?php if(isset($pageLinks) && !empty($pageLinks)){
            foreach($pageLinks as $pl){ ?>
                <li><a href="<?php echo $pl['Link']; ?>"<?php if(isset($pl['id'])) echo ' id="'.$pl['id'].'"'; ?>><i class="fa <?php echo $pl['FontAwesome']; ?>"></i> <?php echo $pl['Name']; ?></a></li>
            <?php }
        } else{ ?>
        <li><a href="<?php echo base_url('user/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <?php } ?>
    </ol>
</section>