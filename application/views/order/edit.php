<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb', array('pageLinks' => array(array('Link' => base_url('order'), 'FontAwesome' => 'fa-list', 'Name' => 'Quản lý Bán hàng')))); ?>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($orderId > 0){ ?>
                    <div class="row">
                        <div class="col-sm-8 no-padding">
                            <div class="box box-default">
                                <div class="box-body">
                                    <div class="table-responsive no-padding divTable">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="text-center">STT</th>
                                                <th class="text-center">Mã sản phẩm</th>
                                                <th>Tên sản phẩm</th>
                                                <th>Loại sản phẩm</th>
                                                <th class="text-center">Số lượng bán</th>
                                                <th class="text-center">Số lượng trả lại</th>
                                                <th>Đơn vị tính</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tbodyProduct">
                                            <?php $i = 0;
                                            foreach($listOrderProducts as $op){
                                                $i++; ?>
                                                <tr id="trProduct_<?php echo $op['ProductId']; ?>" data-id="<?php echo $op['ProductId']; ?>">
                                                    <td class="text-center"><?php echo $i; ?></td>
                                                    <td class="text-center"><?php echo $op['ProductCode']; ?></td>
                                                    <td><?php echo $op['ProductName']; ?></td>
                                                    <td><?php echo $op['ProductTypeName']; ?></td>
                                                    <td class="text-center"><?php echo priceFormat($op['SellQuantity']); ?></td>
                                                    <td class="text-center"><?php echo priceFormat($op['ReturnQuantity']); ?></td>
                                                    <td><?php echo $op['UnitName']; ?></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="box box-default">
                                <div class="box-body">
                                    <div class="box box-widget widget-user-2" id="divCustomer">
                                        <div class="widget-user-header bg-yellow">
                                            <h3 class="widget-user-username"><?php echo $customer['FullName']; ?></h3>
                                            <h5 class="widget-user-desc"><?php echo $this->Mconstants->customerTypes[$customer['CustomerTypeId']]; ?></h5>
                                        </div>
                                        <div class="box-footer no-padding">
                                            <ul class="nav nav-stacked">
                                                <li><a href="javascript:void(0)">Số điện thoại <span class="pull-right badge bg-aqua"><?php echo $customer['PhoneNumber']; ?></span></a></li>
                                                <li><a href="javascript:void(0)">Email <span class="pull-right badge bg-aqua"><?php echo $customer['Email']; ?></span></a></li>
                                                <li><a href="javascript:void(0)">Địa chỉ <span class="pull-right badge bg-aqua" id="spanAddress">
                                                            <?php $wardName = $customer['WardId'] > 0 ? $this->Mwards->getFieldValue(array('WardId' => $customer['WardId']), 'WardName') : '';
                                                            echo $customer['Address'];
                                                            echo empty($customer['Address']) ? $wardName : ', '.$wardName;
                                                            if($customer['DistrictId'] > 0) echo '<br>Quận/ Huyện: '.$this->Mdistricts->getFieldValue(array('DistrictId' => $customer['DistrictId']), 'DistrictName');
                                                            echo '<br/>Tỉnh/ Thành phố: '.$this->Mprovinces->getFieldValue(array('ProvinceId' => $customer['ProvinceId']), 'ProvinceName'); ?>
                                                        </span>
                                                    </a>
                                                    <div class="clearfix"></div>
                                                </li>
                                                <li><a href="javascript:void(0)">Đánh giá vị trí cửa hàng <span class="pull-right badge bg-aqua" id="spanStoreReviewTypeName"><?php echo $this->Mconstants->reviewTypes[$customer['StoreReviewTypeId']]; ?></span></a></li>
                                                <li><a href="javascript:void(0)">Mức độ thân thiết <span class="pull-right badge bg-aqua" id="spanIntimacyTypeName"><?php echo $this->Mconstants->reviewTypes[$customer['IntimacyTypeId']]; ?></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Ngày đặt hàng</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input type="text" class="form-control datepicker" id="orderDate" value="<?php echo ddMMyyyy($order['OrderDate']); ?>" autocomplete="off" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>