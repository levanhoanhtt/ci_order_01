<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb', array('pageLinks' => array(array('Link' => base_url('order/add'), 'FontAwesome' => 'fa-plus', 'Name' => 'Nhập hàng ngày')))); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('order'); ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $this->Mcustomers->selectHtml($listCustomers, 'CustomerId', set_value('CustomerId'), true, '--Khách hàng--'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('customerTypes', 'CustomerTypeId', set_value('CustomerTypeId'), true, '--Phân loại--'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', set_value('ProvinceId'), true, '--Tỉnh/ Thành phố--', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php echo $this->Mdistricts->selectHtml(0, 'DistrictId', $listDistricts, '--Quận/ Huyện--'); ?>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" name="Address" class="form-control" value="<?php echo set_value('Address'); ?>" placeholder="Địa chỉ">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="OrderCode" class="form-control" value="<?php echo set_value('OrderCode'); ?>" placeholder="Mã đơn hàng">
                            </div>
                            <div class="col-sm-6">
                                <select class="form-control select2" id="importProductId">
                                    <option value="0">--Chọn sản phẩm--</option>
                                    <?php foreach($listProducts as $p){ ?>
                                        <option value="<?php echo $p['ProductId']; ?>"<?php if($p['ProductId'] == set_value('ProductId')) echo ' selected="selected"'; ?>><?php echo $p['ProductCode'] . ' - ' . $p['ProductName'] . ' - ' .$this->Mconstants->getObjectValue($listProductTypes, 'ProductTypeId', $p['ProductTypeId'], 'ProductTypeName') ; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listProductTypes, 'ProductTypeId', 'ProductTypeName', 'ProductTypeId', set_value('ProductTypeId'), true, '--Loại sản phẩm--', ' select2'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('dayCompareTypes', 'DayCompareTypeId', set_value('DayCompareTypeId')); ?>
                            </div>
                            <div class="col-sm-3 divDate" id="divBeginDate">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3 divDate" id="divEndDate">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th class="text-center">Mã ĐH</th>
                                <th class="text-center">Ngày bán</th>
                                <th>Tên KH</th>
                                <th>Địa chỉ</th>
                                <th>SĐT</th>
                                <th>Email</th>
                                <th>Sản phẩm</th>
                                <th class="text-center">Số lượng bán</th>
                                <th class="text-center">Số lượng trả lại</th>
                                <th>Đơn vị tính</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder">
                            <?php $i = 0;
                            foreach($listOrders as $o){
                                $i++; ?>
                                <tr id="order_<?php echo $o['OrderId']; ?>">
                                    <td class="text-center"><?php echo $i; ?></td>
                                    <td class="text-center"><a href="<?php echo base_url('order/edit/'.$o['OrderId']); ?>"><?php echo $o['OrderCode']; ?></a></td>
                                    <td class="text-center"><?php echo ddMMyyyy($o['OrderDate']); ?></td>
                                    <td><?php echo $o['FullName']; ?></td>
                                    <td>
                                        <?php echo $o['Address']; ?><?php echo empty($o['Address']) ? $o['WardName'] : ', '.$o['WardName']; ?>
                                        <?php if($o['DistrictId'] > 0) echo '<br>Quận/ Huyện: '.$this->Mconstants->getObjectValue($listDistricts, 'DistrictId', $o['DistrictId'], 'DistrictName');
                                        echo '<br>Tỉnh/ Thành phố: '.$this->Mconstants->getObjectValue($listProvinces, 'ProvinceId', $o['ProvinceId'], 'ProvinceName'); ?>
                                    </td>
                                    <td><?php echo $o['PhoneNumber']; ?></td>
                                    <td><?php echo $o['Email']; ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listProducts, 'ProductId', $o['ProductId'], 'ProductName'); ?></td>
                                    <td class="text-center"><?php echo priceFormat($o['SellQuantity']); ?></td>
                                    <td class="text-center"><?php echo priceFormat($o['ReturnQuantity']); ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listUnits, 'UnitId', $o['UnitId'], 'UnitName'); ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>