<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb', array('pageLinks' => array(array('Link' => base_url('order'), 'FontAwesome' => 'fa-list', 'Name' => 'Quản lý Bán hàng')))); ?>
            <section class="content">
                <div class="row">
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default">
                            <div class="box-body">
                                <div class="form-group">
                                    <select class="form-control select2" id="productId">
                                        <option value="0">--Chọn sản phẩm--</option>
                                        <?php foreach($listProducts as $p){ ?>
                                            <option value="<?php echo $p['ProductId']; ?>" data-id="<?php echo $p['UnitId']; ?>"><?php echo $p['ProductCode'] . ' - ' . $p['ProductName'] . ' - ' .$this->Mconstants->getObjectValue($listProductTypes, 'ProductTypeId', $p['ProductTypeId'], 'ProductTypeName') ; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center">STT</th>
                                            <th class="text-center">Mã sản phẩm</th>
                                            <th>Tên sản phẩm</th>
                                            <th>Loại sản phẩm</th>
                                            <th class="text-center">Số lượng bán</th>
                                            <th class="text-center">Số lượng trả lại</th>
                                            <th>Đơn vị tính</th>
                                            <th style="width: 50px;"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyProduct"></tbody>
                                    </table>
                                </div>
                                <input type="text" hidden="hidden" id="productIndex" value="0">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-default">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="control-label">Khách hàng <span class="required">*</span></label>
                                    <?php $this->Mcustomers->selectHtml($listCustomers, 'CustomerId', set_value('CustomerId'), true, '--Chọn--'); ?>
                                </div>
                                <div id="divCustomerInfo" style="display: none;">
                                    <?php foreach($listCustomers as $c){ ?>
                                        <p id="pCustomer_<?php echo $c['CustomerId'] ?>"
                                           data-name="<?php echo $c['FullName']; ?>"
                                           data-phone="<?php echo $c['PhoneNumber']; ?>"
                                           data-email="<?php echo $c['Email']; ?>"
                                           data-type="<?php echo $c['CustomerTypeId']; ?>"
                                           data-province="<?php echo $c['ProvinceId']; ?>"
                                           data-district="<?php echo $c['DistrictId']; ?>"
                                           data-ward="<?php echo $c['WardId']; ?>"
                                           data-review="<?php echo $c['StoreReviewTypeId']; ?>"
                                           data-intimacy="<?php echo $c['IntimacyTypeId']; ?>"><?php echo $c['Address']; ?></p>
                                    <?php } ?>
                                </div>
                                <div class="box box-widget widget-user-2" id="divCustomer" style="display: none;">
                                    <div class="widget-user-header bg-yellow">
                                        <h3 class="widget-user-username"></h3>
                                        <h5 class="widget-user-desc"></h5>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked"></ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Ngày đặt hàng</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control datepicker" id="orderDate" value="<?php echo date('d/m/Y'); ?>" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group text-right">
                                    <button type="button" class="btn btn-primary" id="btnSubmitOrder">Hoàn thành</button>
                                    <input type="text" hidden="hidden" id="getCustomerInfoUrl" value="<?php echo base_url('customer/getInfo'); ?>">
                                    <input type="text" hidden="hidden" id="orderListUrl" value="<?php echo base_url('order'); ?>/">
                                    <input type="text" hidden="hidden" id="updateOrderUrl" value="<?php echo base_url('order/update'); ?>">
                                    <?php foreach($listProducts as $p){ ?>
                                        <input type="text" hidden="hidden" id="product_<?php echo $p['ProductId']; ?>" data-code="<?php echo $p['ProductCode']; ?>" data-type="<?php echo $p['ProductTypeId']; ?>" data-unit="<?php echo $p['ProductTypeId'] ?>" value="<?php echo $p['ProductName']; ?>">
                                    <?php }
                                    foreach($listProductTypes as $pt){ ?>
                                        <input type="text" hidden="hidden" id="productTypeName_<?php echo $pt['ProductTypeId']; ?>" value="<?php echo $pt['ProductTypeName']; ?>">
                                    <?php }
                                    foreach($listUnits as $u){ ?>
                                        <input type="text" hidden="hidden" id="unitName_<?php echo $u['UnitId']; ?>" value="<?php echo $u['UnitName']; ?>">
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>