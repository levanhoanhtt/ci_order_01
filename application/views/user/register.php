<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url();?>"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php $this->load->view('includes/favicon'); ?>
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/vendor/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="assets/vendor/plugins/pnotify/pnotify.custom.css" />
    <link rel="stylesheet" href="assets/vendor/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="assets/vendor/plugins/pace/pace.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box" style="margin: 10px auto;">
    <div class="login-logo">
        <a href="javascript:void(0)"><b>QL</b> Bán hàng</a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Đăng ký thành viên</p>
        <?php echo form_open('user/saveUser', array('id' => 'userForm', 'autocomplete' => 'off')); ?>
        <div class="form-group has-feedback">
            <input type="text" name="FullName" class="form-control hmdrequired" value="" placeholder="Họ và tên" data-field="Họ và tên">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="text" name="PhoneNumber" id="phoneNumber" class="form-control hmdrequired" value="" placeholder="Di động" data-field="Di động">
            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="text" name="Email" class="form-control" value="" autocomplete="new-password" placeholder="Email/ Facebook">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <?php $this->Mconstants->selectConstants('genders', 'GenderId'); ?>
        </div>
        <div class="form-group has-feedback">
            <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId'); ?>
        </div>
        <div class="form-group has-feedback">
            <input type="text" name="Address" class="form-control hmdrequired" value="" placeholder="Địa chỉ" data-field="Địa chỉ">
            <span class="glyphicon glyphicon-home form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" id="newPass" name="UserPass" class="form-control hmdrequired" autocomplete="new-password" value="" placeholder="Mật khẩu" data-field="Mật khẩu">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" id="rePass" class="form-control hmdrequired" value="" autocomplete="new-password" placeholder="Gõ lại Mật khẩu" data-field="Mật khẩu">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label class="control-label"><input type="checkbox" name="IsSendPass" class="iCheck" checked="checked"> Gửi mật khẩu vào email</label>
                </div>
            </div>
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Đăng ký</button>
                <input type="text" name="UserId" id="userId" hidden="hidden" value="0">
                <input type="text" name="RoleId" id="roleId" hidden="hidden" value="4">
                <input type="text" name="UserStatusId" hidden="hidden" value="<?php echo STATUS_ACTIVED; ?>">
                <input type="text" name="Avatar" hidden="hidden" value="<?php echo NO_IMAGE; ?>">
                <input type="text" name="IsVip" hidden="hidden" value="1">
                <input type="text" hidden="hidden" name="UserTypeName" value="người dùng">
                <input type="text" id="userEditUrl" hidden="hidden" value="<?php echo base_url('user/index'); ?>">
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<script src="assets/vendor/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/plugins/pace/pace.min.js"></script>
<script src="assets/vendor/plugins/pnotify/pnotify.custom.js"></script>
<script src="assets/vendor/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="assets/js/common.js"></script>
<script type="text/javascript" src="assets/js/user_update.js?2"></script>
</body>
</html>