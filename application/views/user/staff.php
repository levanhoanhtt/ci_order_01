<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb', array('pageLinks' => array(array('Link' => base_url('user/add'), 'FontAwesome' => 'fa-plus', 'Name' => 'Thêm nhân viên')))); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('user/staff'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="UserName" class="form-control" value="<?php echo set_value('UserName'); ?>" placeholder="UserName">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="FullName" class="form-control" value="<?php echo set_value('FullName'); ?>" placeholder="Họ và tên">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="Email" class="form-control" value="<?php echo set_value('Email'); ?>" placeholder="Email">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="PhoneNumber" class="form-control" value="<?php echo set_value('PhoneNumber'); ?>" placeholder="Số điện thoại">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('userStatus', 'UserStatusId', set_value('UserStatusId'), true, 'Trạng thái'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', set_value('ProvinceId'), true, 'Tỉnh/ Thành phố', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('roles', 'RoleId', set_value('RoleId'), true, 'Phân cấp'); ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th>UserName</th>
                                <th>Nhân viên</th>
                                <th>SĐT</th>
                                <th>Email</th>
                                <th>Địa chỉ</th>
                                <th class="text-center">Phân cấp</th>
                                <th class="text-center">Trạng thái</th>
                                <th style="width: 77px;"></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyUser">
                            <?php $i = 0;
                            $status = $this->Mconstants->userStatus;
                            $roles = $this->Mconstants->roles;
                            $labelCss = $this->Mconstants->labelCss;
                            foreach($listUsers as $u){
                                $i++; ?>
                                <tr id="user_<?php echo $u['UserId']; ?>">
                                    <td class="text-center"><?php echo $i; ?></td>
                                    <td><a href="<?php echo base_url('user/edit/'.$u['UserId']); ?>"><?php echo $u['UserName']; ?></a></td>
                                    <td><?php echo $u['FullName']; ?></td>
                                    <td><?php echo $u['PhoneNumber']; ?></td>
                                    <td><?php echo $u['Email']; ?></td>
                                    <td><?php echo $u['Address']; ?></td>
                                    <td class="text-center"><span class="<?php echo $labelCss[$u['RoleId']]; ?>"><?php echo $roles[$u['RoleId']]; ?></span></td>
                                    <td class="text-center" id="statusName_<?php echo $u['UserId']; ?>"><span class="<?php echo $labelCss[$u['UserStatusId']]; ?>"><?php echo $status[$u['UserStatusId']]; ?></span></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $u['UserId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <div class="btn-group" id="btnGroup_<?php echo $u['UserId']; ?>">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                            <ul class="dropdown-menu">
                                                <?php foreach($status as $j => $v){ ?>
                                                    <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $u['UserId']; ?>" data-status="<?php echo $j; ?>"><?php echo $v; ?></a></li>
                                                <?php }  ?>
                                            </ul>
                                        </div>
                                        <input type="text" hidden="hidden" id="statusId_<?php echo $u['UserId']; ?>" value="<?php echo $u['UserStatusId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('user/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>