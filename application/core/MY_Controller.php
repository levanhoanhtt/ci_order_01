<?php
defined('BASEPATH') OR exit('No direct script access allowed');

abstract class MY_Controller extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if (function_exists('date_default_timezone_set')) date_default_timezone_set('Asia/Bangkok');
        $user = $this->Musers->get(1);$this->session->set_userdata('user', $user);
    }

    protected function commonData($user, $title, $data = array()){
        $data['user'] = $user;
        $data['title'] = $title;
        $data['listActions'] = $this->Mactions->getByUserId($user['RoleId']);
        return $data;
    }

    protected function checkUserLogin($isApi = false){
        $user = $this->session->userdata('user');
        if($user) return $user;
        else{
            if($isApi) echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            else redirect('admin?redirectUrl='.current_url());
            die();
        }
    }

    protected function loadModel($models = array()){
        foreach($models as $model) $this->load->model($model);
    }

    protected function arrayFromPost($fields) {
        $data = array();
        foreach ($fields as $field) $data[$field] = trim($this->input->post($field));
        return $data;
    }

    protected function formatDateSearch($postData){
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
        if(isset($postData['DayCompareTypeId'])) {
            if ($postData['DayCompareTypeId'] == 2) {
                if (!empty($postData['BeginDate'])) $postData['EndDate'] = $postData['BeginDate'];
            }
            elseif ($postData['DayCompareTypeId'] == 3) unset($postData['BeginDate']);
            elseif ($postData['DayCompareTypeId'] == 4) unset($postData['EndDate']);
        }
        return $postData;
    }

    protected function sendMail($emailFrom, $nameFrom, $emailTo, $subject, $message){
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from($emailFrom, $nameFrom);
        $this->email->to($emailTo);
        $this->email->subject($subject);
        $this->email->message($message);
        if($this->email->send()) return true;
        return false;
    }

}